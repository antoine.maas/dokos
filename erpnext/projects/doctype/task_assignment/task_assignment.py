# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import datetime

import frappe.utils
from frappe.model.document import Document
from frappe.types import DF


def round_duration_to_minutes(dur: DF.Duration | datetime.timedelta | int | None) -> int:
	if not dur:
		return 0
	if isinstance(dur, datetime.timedelta):
		dur = round(dur.total_seconds())
	mini = 1 if dur > 0 else 0
	return max(60 * int(dur / 60), mini)


def datetime_from_date_and_time(date: DF.Date, time: DF.Time):
	if d := frappe.utils.getdate(date):
		if isinstance(time, datetime.time):
			time = time.strftime("%H:%M:%S")
		if t := frappe.utils.get_time(time):
			return datetime.datetime.combine(d, t)


def gettime(t: DF.Time | datetime.timedelta) -> datetime.time | None:
	if isinstance(t, datetime.timedelta):
		t = (datetime.datetime.min + t).time()
	if isinstance(t, datetime.time):
		t = t.strftime("%H:%M:%S")
	if t := frappe.utils.get_time(t):
		return t


@frappe.whitelist()
def compute_daily_duration(start_time: DF.Time | None, end_time: DF.Time | None):
	t1 = start_time and gettime(start_time)
	t2 = end_time and gettime(end_time)
	if t1 and t2:
		if t1.hour == 0 and t2.hour == 0 and t1.minute == 0 and t2.minute == 0:
			return 60 * 60 * 24  # full day

		mins = 60 * (t2.hour - t1.hour) + (t2.minute - t1.minute)
		if mins >= 0:
			return 60 * mins  # duration in seconds


@frappe.whitelist()
def compute_total_duration(
	start_date: DF.Date | None, end_date: DF.Date | None, duration: DF.Duration
):
	"""
	NOTE: The total duration is computed by multiplying the daily duration (end_time - start_time)
	by the number of days between start_date and end_date. This can be wrong for some cases, e.g.
	daylights saving time, leap years, etc.
	"""
	d1 = frappe.utils.getdate(start_date)
	d2 = frappe.utils.getdate(end_date)
	if d1 and d2 and duration:
		n_days = (d2 - d1).days + 1
		return n_days * duration  # total duration in seconds


@frappe.whitelist()
def compute_durations(
	start_date: DF.Date | None,
	end_date: DF.Date | None,
	start_time: DF.Time | None,
	end_time: DF.Time | None,
):
	daily_duration = compute_daily_duration(start_time=start_time, end_time=end_time)
	return {
		"daily": daily_duration,
		"total": compute_total_duration(
			start_date=start_date, end_date=end_date, duration=daily_duration
		),
	}


@frappe.whitelist()
def compute_end_time(
	start_time: DF.Time | datetime.timedelta | None, duration: DF.Duration | None
):
	d = datetime.timedelta(seconds=duration or 0)
	t = start_time and gettime(start_time)
	if d and t:
		dt = datetime.datetime.combine(datetime.date.today(), t) + d
		return dt.time()


class TaskAssignment(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.projects.doctype.task_assignment_row.task_assignment_row import TaskAssignmentRow

		assigned_by: DF.Link | None
		assigned_to: DF.TableMultiSelect[TaskAssignmentRow]
		duration: DF.Duration
		end_date: DF.Date
		end_time: DF.Time | None
		project: DF.Link
		start_date: DF.Date
		start_time: DF.Time | None
		task: DF.Link | None
		total_duration: DF.Duration | None
	# end: auto-generated types

	def validate(self):
		if self.duration:
			self.duration = round_duration_to_minutes(self.duration or 0)
		else:
			self.duration = compute_daily_duration(self.start_time, self.end_time) or self.duration

		# Always compute end_time from duration (now that the duration guaranteed to be set)
		self.end_time = compute_end_time(self.start_time, self.duration)

		start_date = frappe.utils.getdate(self.start_date)
		end_date = frappe.utils.getdate(self.end_date)
		if start_date and end_date and start_date > end_date:
			frappe.throw("End date must be greater than start date")

		self.total_duration = compute_total_duration(self.start_date, self.end_date, self.duration)
