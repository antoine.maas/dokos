// Copyright (c) 2023, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on("Task Assignment", {
	setup(frm) {
		frm.set_query("assigned_to", () => ({
			filters: {
				status: "Active",
			}
		}));

		frm.set_query("task", () => ({
			filters: {
				project: frm.doc.project,
				status: ["!=", "Cancelled"],
			}
		}));
	},

	start_time(frm) {
		frm.trigger("_compute_end_time");
		frm.trigger("_compute_total_duration");
	},

	duration(frm) {
		frm.trigger("_compute_end_time");
		frm.trigger("_compute_total_duration");
	},

	end_time(frm) {
		frm.trigger("_compute_duration");
	},

	_compute_end_time: frappe.utils.debounce(async (frm) => {
		if (!frm.doc.start_time || !Number(frm.doc.duration)) return;
		const res = await frappe.xcall("erpnext.projects.doctype.task_assignment.task_assignment.compute_end_time", {
			start_time: frm.doc.start_time,
			duration: frm.doc.duration,
		});
		frm.doc.end_time = res;
		frm.fields_dict.end_time.refresh();
	}, 100),

	_compute_duration: frappe.utils.debounce(async (frm) => {
		if (!frm.doc.start_time || !frm.doc.end_time || !frm.doc.start_date || !frm.doc.end_date) return;
		const res = await frappe.xcall("erpnext.projects.doctype.task_assignment.task_assignment.compute_durations", {
			start_date: frm.doc.start_date,
			end_date: frm.doc.end_date,
			start_time: frm.doc.start_time,
			end_time: frm.doc.end_time,
		});
		frm.doc.duration = res.daily;
		frm.fields_dict.duration.refresh();
		frm.set_value("total_duration", res.total);
	}, 100),

	_compute_total_duration: frappe.utils.debounce(async (frm) => {
		if (!frm.doc.start_date || !frm.doc.end_date || !Number(frm.doc.duration)) return;
		const res = await frappe.xcall("erpnext.projects.doctype.task_assignment.task_assignment.compute_total_duration", {
			start_date: frm.doc.start_date,
			end_date: frm.doc.end_date,
			duration: frm.doc.duration,
		});
		frm.set_value("total_duration", res);
	}, 100),
});
