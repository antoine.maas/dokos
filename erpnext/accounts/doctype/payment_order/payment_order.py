# Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from itertools import groupby

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import flt, get_url_to_form, nowdate

from erpnext.accounts.party import get_party_account


class PaymentOrder(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.accounts.doctype.payment_order_reference.payment_order_reference import (
			PaymentOrderReference,
		)

		account: DF.Data | None
		amended_from: DF.Link | None
		company: DF.Link
		company_bank: DF.Link | None
		company_bank_account: DF.Link
		naming_series: DF.Literal["PMO-"]
		party: DF.Link | None
		payment_order_type: DF.Literal["", "Payment Request", "Payment Entry"]
		posting_date: DF.Date | None
		references: DF.Table[PaymentOrderReference]
	# end: auto-generated types

	def on_submit(self):
		self.update_payment_status()

	def on_cancel(self):
		self.update_payment_status(cancel=True)

	def update_payment_status(self, cancel=False):
		status = "Payment Ordered"
		if cancel:
			status = "Initiated"

		if self.payment_order_type == "Payment Request":
			ref_field = "status"
			ref_doc_field = frappe.scrub(self.payment_order_type)
		else:
			ref_field = "payment_order_status"
			ref_doc_field = "reference_name"

		for d in self.references:
			frappe.db.set_value(self.payment_order_type, d.get(ref_doc_field), ref_field, status)

	@frappe.whitelist()
	def make_payments_in_batch(self):
		from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry

		sorted_payment_rows = sorted(self.references, key=lambda x: x.supplier)
		for supplier, group in groupby(sorted_payment_rows, lambda x: x.supplier):
			entries = list(group)
			supplier_doc = frappe.get_doc("Supplier", supplier)
			payment_entry = get_payment_entry(
				"Purchase Invoice",
				entries[0].reference_name,
				party_amount=sum(flt(entry.amount) for entry in entries),
				reference_date=self.posting_date,
			)
			payment_entry.mode_of_payment = supplier_doc.mode_of_payment

			for entry in entries[1:]:
				payment_entry.append(
					"references",
					{
						"reference_doctype": entry.reference_doctype,
						"reference_name": entry.reference_name,
						"allocated_amount": entry.amount,
					},
				)

			payment_entry.reference_no = supplier[:10] + "/" + self.name  # Must be limited to 35 characters
			payment_entry.reference_date = self.posting_date
			payment_entry.payment_order = self.name
			payment_entry.insert()
			payment_entry.submit()

	@frappe.whitelist()
	def make_sepa_credit_transfer(self):
		settings = frappe.db.get_value(
			"Sepa Direct Debit Settings", {"company": self.company, "schema": "pain.001.001.03"}
		)
		if not settings:
			frappe.throw(
				_("Please create a {0} document to configure the generation of your XML file").format(
					get_url_to_form(
						"Sepa Direct Debit Settings", "Sepa Direct Debit Settings", _("Sepa Direct Debit Settings")
					)
				)
			)

		sepa_file = frappe.new_doc("Sepa Direct Debit")
		sepa_file.payment_order = self.name
		sepa_file.company = self.company
		sepa_file.settings = settings
		sepa_file.currency = frappe.db.get_value("Account", self.account, "account_currency")

		for reference in self.references:
			sepa_file.append(
				"payment_entries",
				{
					"payment_document": reference.reference_doctype,
					"payment_entry": reference.reference_name,
					"against_account": reference.supplier,
					"amount": reference.amount,
				},
			)

		sepa_file.insert()


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_mop_query(doctype, txt, searchfield, start, page_len, filters):
	return frappe.db.sql(
		""" select mode_of_payment from `tabPayment Order Reference`
		where parent = %(parent)s and mode_of_payment like %(txt)s
		limit %(page_len)s offset %(start)s""",
		{"parent": filters.get("parent"), "start": start, "page_len": page_len, "txt": "%%%s%%" % txt},
	)


@frappe.whitelist()
@frappe.validate_and_sanitize_search_inputs
def get_supplier_query(doctype, txt, searchfield, start, page_len, filters):
	return frappe.db.sql(
		""" select supplier from `tabPayment Order Reference`
		where parent = %(parent)s and supplier like %(txt)s and
		(payment_reference is null or payment_reference='')
		limit %(page_len)s offset %(start)s""",
		{"parent": filters.get("parent"), "start": start, "page_len": page_len, "txt": "%%%s%%" % txt},
	)


@frappe.whitelist()
def make_payment_records(name, supplier, mode_of_payment=None):
	doc = frappe.get_doc("Payment Order", name)
	make_journal_entry(doc, supplier, mode_of_payment)


def make_journal_entry(doc, supplier, mode_of_payment=None):
	je = frappe.new_doc("Journal Entry")
	je.payment_order = doc.name
	je.posting_date = nowdate()
	mode_of_payment_type = frappe._dict(
		frappe.get_all("Mode of Payment", fields=["name", "type"], as_list=1)
	)

	je.voucher_type = "Bank Entry"
	if mode_of_payment and mode_of_payment_type.get(mode_of_payment) == "Cash":
		je.voucher_type = "Cash Entry"

	paid_amt = 0
	party_account = get_party_account("Supplier", supplier, doc.company)
	for d in doc.references:
		if d.supplier == supplier and (not mode_of_payment or mode_of_payment == d.mode_of_payment):
			je.append(
				"accounts",
				{
					"account": party_account,
					"debit_in_account_currency": d.amount,
					"party_type": "Supplier",
					"party": supplier,
					"reference_type": d.reference_doctype,
					"reference_name": d.reference_name,
				},
			)

			paid_amt += d.amount

	je.append("accounts", {"account": doc.account, "credit_in_account_currency": paid_amt})

	je.flags.ignore_mandatory = True
	je.save()
	frappe.msgprint(_("{0} {1} created").format(je.doctype, je.name))
