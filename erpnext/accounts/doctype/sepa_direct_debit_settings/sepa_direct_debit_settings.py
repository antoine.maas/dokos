# Copyright (c) 2019, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document


class SepaDirectDebitSettings(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		bank_account: DF.Link
		company: DF.Link
		company_name: DF.Data
		creditor_identifier: DF.Data | None
		instrument: DF.Literal["CORE", "B2B"]
		mode_of_payment: DF.Link
		reference_prefix: DF.Data
		schema: DF.Literal["pain.008.001.02", "pain.001.001.03"]
	# end: auto-generated types

	def validate(self):
		if self.bank_account:
			iban, swift_number = frappe.db.get_value(
				"Bank Account", self.bank_account, ["iban", "swift_number"]
			)

			if not iban:
				frappe.throw(_("Please register an IBAN in the company's bank account"))

			if not swift_number:
				frappe.throw(_("Please register a SWIFT Number in the company's bank account"))
