// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tax Category', {
	setup: function(frm) {

		frm.set_query('account', 'receivable_accounts', function(doc, cdt, cdn) {
			let d  = locals[cdt][cdn];
			let filters = {
				'account_type': 'Receivable',
				'root_type': 'Asset',
				'company': d.company,
				"is_group": 0
			};

			return {
				filters: filters
			}
		});

		frm.set_query('advance_account', 'receivable_accounts', function (doc, cdt, cdn) {
			let d = locals[cdt][cdn];
			return {
				filters: {
					"account_type": 'Receivable',
					"root_type": "Liability",
					"company": d.company,
					"is_group": 0
				}
			}
		});


		frm.set_query('account', 'payable_accounts', function(doc, cdt, cdn) {
			let d  = locals[cdt][cdn];
			let filters = {
				'account_type': 'Payable',
				'root_type': 'Liability',
				'company': d.company,
				"is_group": 0
			};

			return {
				filters: filters
			}
		});

		frm.set_query('advance_account', 'payable_accounts', function (doc, cdt, cdn) {
			let d = locals[cdt][cdn];
			return {
				filters: {
					"account_type": 'Payable',
					"root_type": "Asset",
					"company": d.company,
					"is_group": 0
				}
			}
		});

	}
});
