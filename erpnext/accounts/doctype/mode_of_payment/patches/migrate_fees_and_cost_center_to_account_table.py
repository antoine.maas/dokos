import frappe


def execute():
	for mop in frappe.get_all("Mode of Payment"):
		mop_doc = frappe.get_doc("Mode of Payment", mop.name)
		for field in ["fee_account", "tax_account"]:
			if mop_doc.get(field):
				company = frappe.db.get_value("Account", mop_doc.get(field), "company")
				for account_row in mop_doc.accounts:
					if account_row.company == company:
						account_row.set(field, mop_doc.get(field))

		if mop_doc.get("cost_center"):
			company = frappe.db.get_value("Cost Center", mop_doc.get("cost_center"), "company")
			for account_row in mop_doc.accounts:
				if account_row.company == company:
					account_row.set(field, mop_doc.get(field))

		mop_doc.save()
