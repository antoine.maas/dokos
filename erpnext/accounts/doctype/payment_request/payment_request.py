# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt


import json
import warnings

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.query_builder.functions import Sum
from frappe.utils import flt, get_url, nowdate
from frappe.utils.background_jobs import enqueue

from erpnext.accounts.doctype.accounting_dimension.accounting_dimension import (
	get_accounting_dimensions,
)
from erpnext.accounts.doctype.payment_entry.payment_entry import (
	get_company_defaults,
	get_payment_entry,
)
from erpnext.accounts.party import get_party_account
from erpnext.accounts.utils import get_account_currency
from erpnext.erpnext_integrations.doctype.integration_references.integration_references import (
	can_make_immediate_payment,
)
from erpnext.utilities import payment_app_import_guard, webshop_app_import_guard


def _get_payment_gateway_controller(*args, **kwargs):
	with payment_app_import_guard():
		from payments.utils import get_payment_gateway_controller

	return get_payment_gateway_controller(*args, **kwargs)


def _get_shopping_cart_settings():
	with webshop_app_import_guard():
		from webshop.webshop.shopping_cart.cart import get_shopping_cart_settings

	return get_shopping_cart_settings()


class PaymentRequest(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		amended_from: DF.Link | None
		base_amount: DF.Currency
		cost_center: DF.Link | None
		currency: DF.Link | None
		customer: DF.Link | None
		email_template: DF.Link | None
		email_to: DF.Data | None
		exchange_rate: DF.Float
		fee_amount: DF.Currency
		grand_total: DF.Currency
		message: DF.TextEditor | None
		mode_of_payment: DF.Link | None
		mute_email: DF.Check
		naming_series: DF.Literal["ACC-PRQ-.YYYY.-.MM.-.DD.-"]
		payment_gateway: DF.Link | None
		payment_gateway_account: DF.Link | None
		payment_key: DF.Data | None
		payment_request_type: DF.Literal["Outward", "Inward"]
		payment_url: DF.Data | None
		print_format: DF.Literal
		project: DF.Link | None
		reference_doctype: DF.Link | None
		reference_name: DF.DynamicLink | None
		status: DF.Literal[
			"", "Draft", "Requested", "Initiated", "Pending", "Paid", "Failed", "Cancelled", "Completed"
		]
		subject: DF.Data | None
		subscription: DF.Link | None
		transaction_date: DF.Date | None
		transaction_reference: DF.Data | None
	# end: auto-generated types

	def before_insert(self):
		self.payment_key = None
		if self.payment_request_type == "Inward":
			self.generate_payment_key()

	def validate(self):
		if self.get("__islocal"):
			self.status = "Draft"
		self.set_payment_request_type()
		self.validate_reference_document()
		self.validate_payment_request_amount()
		if self.payment_request_type == "Inward":
			self.set_customer()
			self.set_payment_url()
			self.set_payment_gateway_from_mode_of_payment()

		if self.payment_request_type == "Outward":
			self.set_supplier()

		if self.email_template and not self.message:
			self.set_message_from_template()

	def set_payment_request_type(self):
		self.payment_request_type = "Inward"
		if self.reference_doctype in frappe.get_hooks("advance_payment_payable_doctypes"):
			self.payment_request_type = "Outward"
			self.mute_email = True

	def validate_reference_document(self):
		if not self.reference_doctype or not self.reference_name:
			frappe.throw(_("To create a Payment Request reference document is required"))

	def validate_payment_request_amount(self):
		existing_payment_request_amount = flt(
			get_existing_payment_request_amount(self.reference_doctype, self.reference_name)
		)

		ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
		if not hasattr(ref_doc, "order_type") or getattr(ref_doc, "order_type") != "Shopping Cart":
			ref_amount = get_amount(ref_doc)
			if not ref_amount:
				frappe.throw(_("Payment Entry is already created"))

			if existing_payment_request_amount + flt(self.grand_total) > ref_amount:
				frappe.throw(
					_("Total Payment Request amount cannot be greater than {0} amount").format(
						self.reference_doctype
					)
				)

	def set_gateway_account(self):
		accounts = frappe.get_all(
			"Payment Gateway Account",
			filters={"payment_gateway": self.payment_gateway, "currency": self.currency},
			fields=["name", "is_default"],
		)

		default_accounts = [x["name"] for x in accounts if x["is_default"]]
		if default_accounts:
			self.db_set("payment_gateway_account", default_accounts[0])
		elif accounts:
			self.db_set("payment_gateway_account", accounts[0]["name"])

	def get_payment_account(self):
		if self.payment_gateway_account:
			return frappe.db.get_value(
				"Payment Gateway Account", self.payment_gateway_account, "payment_account"
			)

	def on_submit(self):
		ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)

		if self.payment_request_type == "Outward":
			self.db_set("status", "Initiated")

			advance_payment_doctypes = frappe.get_hooks(
				"advance_payment_payable_doctypes"
			) + frappe.get_hooks("advance_payment_payable_doctypes")
			if self.reference_doctype in advance_payment_doctypes:
				# set advance payment status
				ref_doc.set_total_advance_paid()

			return

		elif self.payment_request_type == "Inward":
			self.db_set("status", "Requested")
			send_mail = True

			if (
				(hasattr(ref_doc, "order_type") and getattr(ref_doc, "order_type") == "Shopping Cart")
				or self.flags.mute_email
				or self.mute_email
			):
				send_mail = False

			if not self.message:
				self.mute_email = True
				send_mail = False

			if send_mail:
				communication = self.make_communication_entry()
				self.send_email(communication)

			self.update_subscription_mode_of_payment()

			advance_payment_doctypes = frappe.get_hooks(
				"advance_payment_receivable_doctypes"
			) + frappe.get_hooks("advance_payment_payable_doctypes")
			if self.reference_doctype in advance_payment_doctypes:
				# set advance payment status
				ref_doc.set_total_advance_paid()

	def request_phone_payment(self):
		controller = _get_payment_gateway_controller(self.payment_gateway)
		request_amount = self.get_request_amount()

		payment_record = dict(
			reference_doctype="Payment Request",
			reference_docname=self.name,
			payment_reference=self.reference_name,
			request_amount=request_amount,
			sender=self.email_to,
			currency=self.currency,
			payment_gateway=self.payment_gateway,
		)

		controller.validate_transaction_currency(self.currency)
		controller.request_for_payment(**payment_record)

	def on_cancel(self):
		self.check_if_payment_entry_exists()
		self.set_as_cancelled()

		advance_payment_doctypes = frappe.get_hooks(
			"advance_payment_receivable_doctypes"
		) + frappe.get_hooks("advance_payment_payable_doctypes")
		if self.reference_doctype in advance_payment_doctypes:
			# set advance payment status
			ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
			ref_doc.set_total_advance_paid()

	def make_invoice(self):
		if self.reference_doctype == "Sales Order":
			from erpnext.selling.doctype.sales_order.sales_order import make_sales_invoice

			si = make_sales_invoice(self.reference_name, ignore_permissions=True)
			si.allocate_advances_automatically = True
			si.insert(ignore_permissions=True)
			si.submit()

	@frappe.whitelist()
	def check_if_immediate_payment_is_autorized(self):
		if not self.get("payment_gateway"):
			return False

		return self.check_immediate_payment_for_gateway()

	def check_immediate_payment_for_gateway(self) -> bool:
		"""Returns a boolean"""
		controller = _get_payment_gateway_controller(self.payment_gateway)
		return can_make_immediate_payment(self, controller)

	@frappe.whitelist()
	def process_payment_immediately(self):
		if (
			frappe.conf.mute_payment_gateways
			or not self.get("payment_gateway")
			or not self.check_immediate_payment_for_gateway()
		):
			return

		return self.get_immediate_payment_for_gateway(self.payment_gateway)

	def get_immediate_payment_for_gateway(self, gateway):
		controller = _get_payment_gateway_controller(gateway)
		if hasattr(controller, "immediate_payment_processing"):
			result = controller.immediate_payment_processing(
				reference=self.name,
				customer=self.get_customer(),
				amount=flt(self.grand_total, self.precision("grand_total")),
				currency=self.currency,
				description=self.subject,
				metadata={
					"reference_doctype": self.doctype,
					"reference_name": self.name,
				},
			)
			if result:
				self.db_set("transaction_reference", result, commit=True)
				self.db_set("status", "Pending", commit=True)
				return result
			else:
				frappe.throw(_("Payment cannot be processed immediately for this payment request."))

	def generate_payment_key(self):
		self.payment_key = frappe.generate_hash(self.as_json())

	def set_payment_url(self):
		self.payment_url = get_url("/payments?link={0}".format(self.payment_key))

	def set_customer(self):
		if customer := self.get_customer():
			self.customer = customer
			self.set_mode_of_payment("Customer", self.customer)

	def set_supplier(self):
		if supplier := self.get_supplier():
			self.supplier = supplier
			self.set_mode_of_payment("Supplier", self.supplier)

	def set_mode_of_payment(self, party_type, party):
		if not self.mode_of_payment and (
			mop := frappe.db.get_value(party_type, party, "mode_of_payment")
		):
			self.mode_of_payment = mop

	def get_customer(self):
		if frappe.db.has_column(self.reference_doctype, "customer"):
			return frappe.db.get_value(self.reference_doctype, self.reference_name, "customer")

	def get_supplier(self):
		if frappe.db.has_column(self.reference_doctype, "supplier"):
			return frappe.db.get_value(self.reference_doctype, self.reference_name, "supplier")

	def register_default_payment_method(self):
		for dt in ["Customer", "Supplier", "Subscription"]:
			if self.get(dt.lower()):
				frappe.db.set_value(dt, self.get(dt.lower()), "mode_of_payment", self.mode_of_payment)

	@property
	def reference_document(self):
		return frappe.get_doc(self.reference_doctype, self.reference_name)

	def get_payment_url(self, payment_gateway):
		self.set_gateway_account()

		data = frappe._dict(
			{
				"title": self.reference_document.get("company") or self.subject,
				"customer": self.customer,
				"customer_name": frappe.db.get_value("Customer", self.customer, "customer_name"),
			}
		)

		controller = _get_payment_gateway_controller(payment_gateway)
		controller.validate_transaction_currency(self.currency)

		if hasattr(controller, "validate_minimum_transaction_amount"):
			controller.validate_minimum_transaction_amount(self.currency, self.grand_total)

		# All keys kept for backward compatibility
		return controller.get_payment_url(
			**{
				"amount": flt(self.grand_total, self.precision("grand_total")),
				"title": data.title,
				"description": self.subject or data.title,
				"reference_doctype": "Payment Request",
				"reference_docname": self.name,
				"payer_email": self.email_to or frappe.session.user,
				"payer_name": data.customer_name,
				"order_id": self.name,
				"currency": self.currency,
				"payment_key": self.payment_key,
			}
		)

	@frappe.whitelist()
	def set_as_paid(self, reference_no=None):
		if reference_no and (
			existing_pe := frappe.db.get_value(
				"Payment Entry", filters={"docstatus": 1, "reference_no": reference_no}
			)
		):
			return frappe.get_doc("Payment Entry", existing_pe)

		frappe.flags.mute_messages = True
		if reference_no:
			self.register_customer(reference_no)

		payment_entry = self.create_payment_entry(reference_no=reference_no)
		self.make_invoice()
		frappe.flags.mute_messages = False

		self.db_set("status", "Paid", commit=True)

		return payment_entry

	def register_customer(self, reference_no):
		if frappe.flags.in_test:
			return

		controller = _get_payment_gateway_controller(self.payment_gateway)
		if frappe.db.exists("Integration References", dict(customer=self.customer)):
			doc = frappe.get_doc("Integration References", dict(customer=self.customer))
		else:
			doc = frappe.new_doc("Integration References")

		if controller.doctype == "Stripe Settings":
			doc.stripe_customer_id = controller.get_customer_id(reference_no)
			if not doc.stripe_customer_id:
				return
			doc.stripe_settings = controller.name

		doc.customer = self.customer
		doc.save(ignore_permissions=True)

	def create_payment_entry(self, submit=True, reference_no=None):
		"""create entry"""
		frappe.flags.ignore_account_permission = True
		frappe.flags.ignore_permissions = True

		ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
		ref_doc.currency = ref_doc.get("currency", self.currency)

		company_currency = ref_doc.get(
			"company_currency", frappe.db.get_value("Company", ref_doc.company, "default_currency")
		)

		mode_of_payment_defaults = (
			frappe.db.get_value(
				"Mode of Payment Account",
				dict(parent=self.mode_of_payment, company=ref_doc.company),
				["fee_account", "tax_account", "cost_center"],
				as_dict=1,
			)
			if self.mode_of_payment
			else dict()
		)

		if self.reference_doctype == "Sales Invoice":
			party_account = ref_doc.debit_to
		else:
			party_account = get_party_account(
				"Customer", ref_doc.get("customer"), ref_doc.company, tax_category=ref_doc.get("tax_category")
			)  # @dokos

		party_account_currency = ref_doc.get("party_account_currency") or get_account_currency(
			party_account
		)

		bank_amount = self.grand_total
		if party_account_currency == company_currency and party_account_currency != self.currency:
			party_amount = ref_doc.get("base_rounded_total") or ref_doc.get("base_grand_total")
		else:
			party_amount = self.grand_total

		payment_entry = get_payment_entry(
			self.reference_doctype,
			self.reference_name,
			party_amount=party_amount,
			bank_account=self.get_payment_account(),
			bank_amount=bank_amount,
		)

		payment_entry.setup_party_account_field()
		payment_entry.set_missing_values()
		payment_entry.set_exchange_rate()

		payment_entry.update(
			{
				"reference_no": reference_no if reference_no else (self.transaction_reference or self.name),
				"reference_date": nowdate(),
				"mode_of_payment": self.mode_of_payment,
				"remarks": _("Payment Entry against {0} {1} via Payment Request {2}").format(
					self.reference_doctype, self.reference_name, self.name
				),
			}
		)

		self.get_payment_gateway_fees(reference_no)

		total_fee_amount = 0.0
		for value in ["fee", "tax"]:
			if (
				self.get(f"{value}_amount")
				and mode_of_payment_defaults.get(f"{value}_account")
				and mode_of_payment_defaults.get("cost_center")
			):
				total_fee_amount += flt(self.get(f"{value}_amount")) * flt(self.get("target_exchange_rate", 1))

				payment_entry.append(
					"deductions",
					{
						"account": mode_of_payment_defaults.get(f"{value}_account"),
						"cost_center": mode_of_payment_defaults.get("cost_center"),
						"amount": self.get(f"{value}_amount"),
					},
				)

		payment_entry.update(
			{
				"paid_amount": flt(self.base_amount or self.grand_total) - total_fee_amount,
				"received_amount": flt(self.grand_total) - total_fee_amount,
			}
		)

		payment_entry.set_amounts()

		# Update dimensions
		payment_entry.update(
			{
				"cost_center": self.get("cost_center"),
				"project": self.get("project"),
			}
		)

		for dimension in get_accounting_dimensions():
			payment_entry.update({dimension: self.get(dimension)})

		if payment_entry.difference_amount:
			company_details = get_company_defaults(ref_doc.company)

			payment_entry.append(
				"deductions",
				{
					"account": company_details.exchange_gain_loss_account,
					"cost_center": company_details.cost_center,
					"amount": payment_entry.difference_amount,
				},
			)

		payment_entry.payment_request = self.name

		if submit:
			payment_entry.insert(ignore_permissions=True)
			payment_entry.submit()

		return payment_entry

	def get_payment_gateway_fees(self, reference_no=None):
		if self.payment_gateway and reference_no:
			controller = _get_payment_gateway_controller(self.payment_gateway)
			if hasattr(controller, "get_transaction_fees"):
				transaction_fees = controller.get_transaction_fees(reference_no)
				if not self.fee_amount:
					self.fee_amount = 0.0

				self.fee_amount += transaction_fees.fee_amount
				self.base_amount = transaction_fees.base_amount or self.base_amount
				self.tax_amount = transaction_fees.get("tax_amount")
				if transaction_fees.target_exchange_rate:
					self.target_exchange_rate = transaction_fees.target_exchange_rate

	def send_email(self, communication=None):
		"""send email with payment link"""
		email_args = {
			"recipients": self.email_to,
			"sender": frappe.session.user
			if frappe.session.user not in ("Administrator", "Guest")
			else None,
			"subject": self.subject,
			"message": self.message,
			"now": True,
			"communication": communication.name if communication else None,
			"attachments": [
				frappe.attach_print(
					self.reference_doctype,
					self.reference_name,
					file_name=self.reference_name,
					print_format=self.print_format,
				)
			]
			if self.print_format
			else [],
		}

		enqueue(method=frappe.sendmail, queue="short", timeout=300, is_async=True, **email_args)

	def set_failed(self):
		self.db_set("status", "Failed")

	def set_as_cancelled(self):
		self.db_set("status", "Cancelled")

	def check_if_payment_entry_exists(self):
		if self.status == "Paid":
			if frappe.get_all(
				"Payment Entry Reference",
				filters={"reference_name": self.reference_name, "docstatus": ["<", 2]},
				fields=["parent"],
				limit=1,
			):
				frappe.throw(_("A payment entry for this reference exists already"), title=_("Error"))

	def make_communication_entry(self):
		"""Make communication entry"""
		try:
			comm = frappe.get_doc(
				{
					"doctype": "Communication",
					"communication_medium": "Email",
					"recipients": self.email_to,
					"subject": self.subject,
					"content": self.message,
					"sent_or_received": "Sent",
					"reference_doctype": self.reference_doctype,
					"reference_name": self.reference_name,
				}
			)
			comm.insert(ignore_permissions=True)

			return comm
		except Exception:
			comm.log_error(_("Payment request communication creation error"))

	def get_payment_success_url(self):
		return self.payment_success_url

	def on_payment_authorized(self, status=None, reference_no=None):
		if not status:
			return

		PAID_STATUSES = ("Authorized", "Completed", "Paid")
		curr_status, next_status = self.status, status
		is_not_draft = not self.docstatus.is_draft()

		if next_status in PAID_STATUSES:
			self.run_method("set_as_paid", reference_no)
		elif (curr_status == "Requested") and (next_status == "Pending") and is_not_draft:
			self.db_set("status", "Pending", commit=True)
		elif (curr_status in ("Pending", "Requested")) and (next_status == "Payment Method Registered"):
			if curr_status == "Requested":
				self.db_set("status", "Pending", commit=True)
			self.run_method("set_payment_method_registered")

		self.db_set("transaction_reference", reference_no, commit=True)

		if curr_status in ("Pending", "Paid"):
			self.register_default_payment_method()

		advance_payment_doctypes = frappe.get_hooks(
			"advance_payment_receivable_doctypes"
		) + frappe.get_hooks("advance_payment_payable_doctypes")
		if self.reference_doctype in advance_payment_doctypes:
			# set advance payment status
			self.reload()
			ref_doc = frappe.get_doc(self.reference_doctype, self.reference_name)
			ref_doc.set_advance_payment_status(
				advance_paid=self.grand_total if self.status == "Paid" else 0.0,
				order_total=ref_doc.get("grand_total"),
			)

		return self.get_redirection()

	def set_payment_method_registered(self):
		"""Called when payment method is registered for off-session payments"""
		self.process_payment_immediately()

	def get_redirection(self):
		redirect_to = "no-redirection"

		try:
			# if shopping cart enabled and in session
			shopping_cart_settings = _get_shopping_cart_settings()

			if (
				shopping_cart_settings.get("enabled")
				and hasattr(frappe.local, "session")
				and frappe.local.session.user != "Guest"
			):

				success_url = shopping_cart_settings.get("payment_success_url")
				if success_url:
					redirect_to = ({"Orders": "/orders", "Invoices": "/invoices", "My Account": "/me"}).get(
						success_url, "/me"
					)
				else:
					redirect_to = get_url("/orders/{0}".format(self.reference_name))
		except Exception:
			frappe.clear_messages()
			redirect_to = "payment-success"
			controller = _get_payment_gateway_controller(self.payment_gateway)
			if controller.doctype == "Stripe Settings" and controller.redirect_url:
				redirect_to = controller.redirect_url

		return redirect_to

	def set_message_from_template(self):
		data = frappe._dict(get_message(self, self.email_template))
		self.subject, self.message = data.subject, data.message

	def set_payment_gateway_from_mode_of_payment(self):
		if self.mode_of_payment:
			if payment_gateway := frappe.get_cached_value(
				"Mode of Payment", self.mode_of_payment, "payment_gateway"
			):
				self.payment_gateway = payment_gateway

	def update_subscription_mode_of_payment(self):
		if self.subscription and self.mode_of_payment:
			if (
				mode_of_payment := frappe.db.get_value("Subscription", self.subscription, "mode_of_payment")
			) and mode_of_payment != self.mode_of_payment:
				frappe.db.set_value("Subscription", self.subscription, "mode_of_payment", self.mode_of_payment)


@frappe.whitelist(allow_guest=True)
def make_payment_request(*args, **kwargs):
	"""Make payment request"""
	args = frappe._dict(kwargs)
	ref_doc = frappe.get_doc(args.dt, args.dn)
	grand_total = flt(args.grand_total) or get_amount(ref_doc)

	if args.loyalty_points and args.dt == "Sales Order":
		from erpnext.accounts.doctype.loyalty_program.loyalty_program import validate_loyalty_points

		loyalty_amount = validate_loyalty_points(ref_doc, int(args.loyalty_points))
		frappe.db.set_value(
			"Sales Order", args.dn, "loyalty_points", int(args.loyalty_points), update_modified=False
		)
		frappe.db.set_value(
			"Sales Order", args.dn, "loyalty_amount", loyalty_amount, update_modified=False
		)
		grand_total = grand_total - loyalty_amount

	existing_payment_request = None
	if args.order_type == "Shopping Cart":
		existing_payment_request = frappe.db.get_value(
			"Payment Request",
			{"reference_doctype": args.dt, "reference_name": args.dn, "docstatus": ("!=", 2)},
		)

	if existing_payment_request:
		frappe.db.set_value(
			"Payment Request", existing_payment_request, "grand_total", grand_total, update_modified=False
		)
		pr = frappe.get_doc("Payment Request", existing_payment_request)

	else:
		if args.order_type != "Shopping Cart" and args.dt != "Subscription":
			existing_payment_request_amount = get_existing_payment_request_amount(args.dt, args.dn)

			if existing_payment_request_amount:
				grand_total -= existing_payment_request_amount

		email_to = args.recipient_id or ref_doc.get("contact_email") or ref_doc.owner
		if email_to in ["Administrator", "Guest"]:
			email_to = None

		pr = frappe.new_doc("Payment Request")
		pr.update(
			{
				"currency": args.currency or ref_doc.currency,
				"grand_total": grand_total,
				"email_to": email_to,
				"subject": _("Payment Request for {0}").format(args.dn),
				"reference_doctype": args.dt,
				"reference_name": args.dn,
				"email_template": args.email_template,
				"print_format": args.print_format,
				"subscription": args.subscription,
				"payment_request_type": "Outward"
				if ref_doc.doctype in frappe.get_hooks("advance_payment_payable_doctypes")
				else "Inward",
				"mode_of_payment": args.get("mode_of_payment"),
			}
		)

		# Update dimensions
		pr.update(
			{
				"cost_center": ref_doc.get("cost_center"),
				"project": ref_doc.get("project"),
			}
		)

		for dimension in get_accounting_dimensions():
			pr.update({dimension: ref_doc.get(dimension)})

		if args.order_type == "Shopping Cart" or args.mute_email:
			pr.flags.mute_email = True

		if args.get("payment_gateway") or args.get("mode_of_payment"):
			gateway_account = get_gateway_details(args) or frappe._dict()
			pr.update(
				{
					"payment_gateway_account": gateway_account.get("name"),
					"payment_gateway": gateway_account.get("payment_gateway"),
				}
			)

		if args.submit_doc:
			pr.insert(ignore_permissions=True)
			pr.submit()

	if args.order_type == "Shopping Cart" and not args.return_doc:
		pr.set_payment_url()
		frappe.db.commit()
		frappe.local.response["type"] = "redirect"
		frappe.local.response["location"] = pr.payment_url

	if args.return_doc:
		return pr

	return pr.as_dict()


@frappe.whitelist()
def get_reference_amount(doctype, docname):
	ref_doc = frappe.get_doc(doctype, docname)
	return get_amount(ref_doc)


def get_amount(ref_doc, payment_account=None):
	"""get amount based on doctype"""
	dt = ref_doc.doctype
	if dt in ["Sales Order", "Purchase Order"]:
		grand_total = flt(ref_doc.rounded_total or ref_doc.grand_total) - flt(ref_doc.advance_paid)
	elif dt in ["Sales Invoice", "Purchase Invoice"]:
		if not ref_doc.get("is_pos"):
			if ref_doc.party_account_currency == ref_doc.currency:
				grand_total = flt(ref_doc.outstanding_amount)
			else:
				grand_total = flt(ref_doc.outstanding_amount) / ref_doc.conversion_rate
		elif dt == "Sales Invoice":
			for pay in ref_doc.payments:
				if pay.type == "Phone" and pay.account == payment_account:
					grand_total = pay.amount
					break
	elif dt == "POS Invoice":
		for pay in ref_doc.payments:
			if pay.type == "Phone" and pay.account == payment_account:
				grand_total = pay.amount
				break
	elif dt == "Fees":
		grand_total = ref_doc.outstanding_amount

	if grand_total > 0:
		return grand_total

	else:
		frappe.throw(_("There is no outstanding amount for this reference"))


def get_existing_payment_request_amount(ref_dt, ref_dn):
	existing_payment_request_amount = frappe.db.sql(
		"""
		select sum(grand_total)
		from `tabPayment Request`
		where
			reference_doctype = %s
			and reference_name = %s
			and docstatus = 1
			and status != 'Paid'
	""",
		(ref_dt, ref_dn),
	)
	return flt(existing_payment_request_amount[0][0]) if existing_payment_request_amount else 0


def get_gateway_details(args):  # nosemgrep
	"""return gateway and payment account of default payment gateway"""
	filters = {}
	if args.get("currency"):
		filters.update({"currency": args.get("currency")})

	if args.get("payment_gateway"):
		filters.update({"payment_gateway": args.get("payment_gateway")})
		return get_payment_gateway_account(filters)

	if args.get("mode_of_payment") and (
		payment_gateway := frappe.db.get_value(
			"Mode of Payment", args.get("mode_of_payment"), "payment_gateway"
		)
	):
		filters.update({"payment_gateway": payment_gateway})
		return get_payment_gateway_account(filters)

	if args.order_type == "Shopping Cart":
		payment_gateway_account = _get_shopping_cart_settings().payment_gateway_account
		return get_payment_gateway_account(payment_gateway_account)

	filters.update({"is_default": 1})
	gateway_account = get_payment_gateway_account(filters)

	return gateway_account


def get_payment_gateway_account(args):
	return frappe.db.get_value(
		"Payment Gateway Account", args, ["name", "payment_gateway"], as_dict=1
	)


@frappe.whitelist()
def get_print_format_list(ref_doctype):
	print_format_list = ["Standard"]

	print_format_list.extend(
		[p.name for p in frappe.get_all("Print Format", filters={"doc_type": ref_doctype})]
	)

	return {"print_format": print_format_list}


@frappe.whitelist(allow_guest=True)
def resend_payment_email(docname):
	doc = frappe.get_doc("Payment Request", docname)
	communication = doc.make_communication_entry()
	return doc.send_email(communication)


@frappe.whitelist()
def make_payment_entry(docname):
	doc = frappe.get_doc("Payment Request", docname)
	return doc.create_payment_entry(submit=False, reference_no=doc.transaction_reference).as_dict()


def make_status_as_paid(doc, method):
	warnings.warn("make_status_as_paid will be deprecated since it is error prone.", FutureWarning)
	for ref in doc.references:
		payment_request_name = frappe.db.get_value(
			"Payment Request",
			{
				"reference_doctype": ref.reference_doctype,
				"reference_name": ref.reference_name,
				"docstatus": 1,
			},
		)

		if payment_request_name:
			doc = frappe.get_doc("Payment Request", payment_request_name)
			if doc.status != "Paid":
				doc.db_set("status", "Paid")
				frappe.db.commit()


@frappe.whitelist()
def make_status_as_completed(name):
	frappe.db.set_value("Payment Request", name, "status", "Completed")


@frappe.whitelist()
def get_message(doc, template):
	"""return message with payment gateway link"""
	payment_can_be_processed_immediately = None

	if isinstance(doc, str):
		doc = json.loads(doc)
	elif isinstance(doc, Document):
		payment_can_be_processed_immediately = doc.check_if_immediate_payment_is_autorized()
		doc = doc.as_dict()

	context = dict(
		doc,
		**{
			"doc": doc,
			"reference": frappe.get_doc(doc.get("reference_doctype"), doc.get("reference_name")),
			"payment_link": doc.get("payment_url"),
			"payment_can_be_processed_immediately": bool(payment_can_be_processed_immediately),
		},
	)

	email_template = frappe.get_doc("Email Template", template)

	return {
		"subject": frappe.render_template(email_template.subject, context),
		"message": frappe.render_template(email_template.response, context),
	}


@frappe.whitelist()
def check_if_immediate_payment_is_autorized(payment_request):
	return frappe.get_doc(
		"Payment Request", payment_request
	).check_if_immediate_payment_is_autorized()


@frappe.whitelist()
def make_payment_order(source_name, target_doc=None):
	from frappe.model.mapper import get_mapped_doc

	def set_missing_values(source, target):
		target.payment_order_type = "Payment Request"
		bank_account = source.bank_account or frappe.db.get_value(
			"Supplier", source.supplier, "default_bank_account"
		)
		target.append(
			"references",
			{
				"reference_doctype": source.reference_doctype,
				"reference_name": source.reference_name,
				"amount": source.grand_total,
				"supplier": source.supplier,
				"payment_request": source_name,
				"mode_of_payment": source.mode_of_payment,
				"bank_account": bank_account,
				"account": source.account,
			},
		)

	doclist = get_mapped_doc(
		"Payment Request",
		source_name,
		{
			"Payment Request": {
				"doctype": "Payment Order",
			}
		},
		target_doc,
		set_missing_values,
	)

	return doclist


def validate_payment(doc, method=None):
	if doc.reference_doctype != "Payment Request" or (
		frappe.db.get_value(doc.reference_doctype, doc.reference_docname, "status") != "Paid"
	):
		return

	frappe.throw(
		_("The Payment Request {0} is already paid, cannot process payment twice").format(
			doc.reference_docname
		)
	)


def get_paid_amount_against_order(dt, dn):
	pe_ref = frappe.qb.DocType("Payment Entry Reference")
	if dt == "Sales Order":
		inv_dt, inv_field = "Sales Invoice Item", "sales_order"
	else:
		inv_dt, inv_field = "Purchase Invoice Item", "purchase_order"
	inv_item = frappe.qb.DocType(inv_dt)
	return (
		frappe.qb.from_(pe_ref)
		.select(
			Sum(pe_ref.allocated_amount),
		)
		.where(
			(pe_ref.docstatus == 1)
			& (
				(pe_ref.reference_name == dn)
				| pe_ref.reference_name.isin(
					frappe.qb.from_(inv_item).select(inv_item.parent).where(inv_item[inv_field] == dn).distinct()
				)
			)
		)
	).run()[0][0] or 0
