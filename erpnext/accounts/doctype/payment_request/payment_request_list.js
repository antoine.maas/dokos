frappe.listview_settings['Payment Request'] = {
	add_fields: ["status"],
	get_indicator: function(doc) {
		if(doc.status == "Draft") {
			return [__("Draft"), "darkgrey", "status,=,Draft"];
		}
		else if(doc.status == "Initiated") {
			return [__("Initiated"), "blue", "status,=,Initiated"];
		}
		else if(doc.status == "Requested") {
			return [__("Requested"), "blue", "status,=,Requested"];
		}
		else if(doc.status == "Paid") {
			return [__("Paid"), "green", "status,=,Paid"];
		}
		else if(doc.status == "Failed") {
			return [__("Failed"), "red", "status,=,Failed"];
		}
		else if(doc.status == "Cancelled") {
			return [__("Cancelled"), "gray", "status,=,Cancelled"];
		}
		else if(doc.status == "Pending") {
			return [__("Pending"), "orange", "status,=,Pending"];
		}
		else if(doc.status == "Completed") {
			return [__("Completed"), "green", "status,=,Completed"];
		}
	}
}
