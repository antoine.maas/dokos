from typing import TYPE_CHECKING

import frappe
from frappe.utils.data import add_days, add_to_date, cint, flt, getdate, nowdate

if TYPE_CHECKING:
	from erpnext.accounts.doctype.subscription.subscription import Subscription


class SubscriptionPeriod:
	def __init__(self, subscription: "Subscription", start=None, end=None):
		self.subscription = subscription
		self.recurrence_period = frappe.get_cached_doc(
			"Recurrence Period",
			self.subscription.recurrence_period,
		)
		self.start = start or self.subscription.current_invoice_start or self.subscription.start
		self.end = end or self.subscription.current_invoice_end

	def validate(self):
		current_invoice_start = self.get_current_invoice_start()
		if self.subscription.current_invoice_start != current_invoice_start:
			self.subscription.set_state("previous_period_start", self.subscription.current_invoice_start)
			self.subscription.current_invoice_start = current_invoice_start
			self.subscription.db_set("current_invoice_start", current_invoice_start, update_modified=False)

		current_invoice_end = self.get_current_invoice_end()
		if self.subscription.current_invoice_end != current_invoice_end:
			self.subscription.set_state("previous_period_end", self.subscription.current_invoice_end)
			self.subscription.current_invoice_end = current_invoice_end
			self.subscription.db_set("current_invoice_end", current_invoice_end, update_modified=False)

			is_new = self.subscription.is_new() or self.subscription.flags.is_new_subscription
			if not is_new and not self.subscription.order_generation_days_before_period:
				self.subscription.clear_period_state()

	def get_current_invoice_start(self):
		if SubscriptionStateManager(self.subscription).is_trial() or (
			self.subscription.cancellation_date
			and getdate(self.subscription.cancellation_date) < getdate(nowdate())
		):
			return None
		elif not self.subscription.current_invoice_start or not self.start:
			return (
				max(getdate(self.subscription.start), add_days(getdate(self.subscription.trial_period_end), 1))
				if self.subscription.trial_period_end
				else self.subscription.start
			)
		elif (
			self.subscription.get_doc_before_save()
			and self.subscription.get_doc_before_save().recurrence_period
			!= self.subscription.recurrence_period
		):
			self.subscription.load_doc_before_save()
			return (
				add_days(getdate(self.end), 1)
				if getdate(nowdate()) > getdate(self.end)
				else getdate(self.start)
			)
		elif self.subscription.get_doc_before_save() and getdate(
			self.subscription.get_doc_before_save().trial_period_end
		) != getdate(self.subscription.trial_period_end):
			self.subscription.load_doc_before_save()
			return (
				max(getdate(self.subscription.start), add_days(getdate(self.subscription.trial_period_end), 1))
				if self.subscription.trial_period_end
				else self.subscription.start
			)
		elif getdate(self.subscription.current_invoice_end) < getdate(nowdate()):
			return self.get_next_period_start()
		else:
			return (
				max(
					getdate(self.subscription.current_invoice_start),
					add_days(getdate(self.subscription.trial_period_end), 1),
				)
				if self.subscription.trial_period_end
				else self.subscription.current_invoice_start
			)

	def get_current_invoice_end(self):
		if SubscriptionStateManager(self.subscription).is_trial():
			return None
		elif self.subscription.cancellation_date:
			return self.subscription.cancellation_date
		elif self.subscription.is_new():
			return self.get_next_period_end()
		elif (
			self.subscription.get_doc_before_save()
			and self.subscription.get_doc_before_save().current_invoice_start
			!= self.subscription.current_invoice_start
		):
			return self.get_next_period_end()
		elif getdate(self.subscription.current_invoice_end) < getdate(
			self.subscription.current_invoice_start
		):
			return self.get_next_period_end()
		else:
			return self.subscription.current_invoice_end

	def get_next_period_start(self):
		if not self.subscription.current_invoice_start:
			return max(
				getdate(self.subscription.start), add_days(getdate(self.subscription.trial_period_end), 1)
			)

		if getdate(self.subscription.current_invoice_end) < getdate(nowdate()):
			return add_days(self.get_next_period_end(), 1)

	def get_next_period_end(self):
		return self.recurrence_period.get_end_date(
			self.subscription.current_invoice_start, self.subscription.invoicing_day
		)

	def get_next_invoice_date(self):
		if (
			self.subscription.generate_invoice_at_period_start
			and self.subscription.trial_period_end
			and getdate(self.subscription.trial_period_end) >= getdate(nowdate())
		):
			return add_days(self.subscription.trial_period_end, 1)
		elif (
			not (self.subscription.generate_invoice_at_period_start)
			and self.subscription.trial_period_end
			and getdate(self.subscription.trial_period_end) >= getdate(nowdate())
		):
			return add_to_date(
				add_days(self.subscription.trial_period_end, 1),
				**self.recurrence_period.get_billing_cycle_data()
			)
		elif not (self.subscription.generate_invoice_at_period_start):
			return add_days(self.subscription.current_invoice_end, 1)
		elif self.subscription.current_invoice_start:
			if self.subscription.get_state_value("sales_invoice"):
				return add_days(self.subscription.current_invoice_end, 1)
			else:
				return self.subscription.current_invoice_start


class SubscriptionStateManager:
	def __init__(self, subscription=None):
		self.subscription = subscription

		self.subscription_state = self.subscription.get_state()

		self.sales_order = (
			frappe.get_cached_doc("Sales Order", self.subscription_state.get("sales_order"))
			if self.subscription_state.get("sales_order")
			else None
		)

		self.sales_invoice = (
			frappe.get_cached_doc("Sales Invoice", self.subscription_state.get("sales_invoice"))
			if self.subscription_state.get("sales_invoice")
			else None
		)

	def set_status(self):
		status = "Active"
		if (
			self.subscription.status == "Billing failed"
			and self.subscription.get_doc_before_save()
			and self.subscription.get_doc_before_save().status != "Billing failed"
		):
			status = "Billing failed"
		elif self.is_cancelled() and self.is_billable():
			status = "Cancelled and billable"
		elif self.is_cancelled():
			status = "Cancelled"
		elif not self.is_cancelled() and self.is_trial():
			status = "Trial"
		elif not self.is_cancelled() and not self.is_trial():
			if self.is_payable():
				status = "Payable"
			elif self.is_billable():
				status = "Billable"
			elif self.is_draft():
				status = "Draft invoices"
			elif flt(self.subscription.outstanding_amount) > 0:
				status = "Unpaid"
			elif self.is_paid():
				status = "Paid"

		if status != self.subscription.status:
			self.subscription.set_state("previous_status", self.subscription.status)
			self.subscription.db_set("status", status)

			if status == "Billable" and (
				self.order_can_be_generated_before_period_start()
				or self.order_can_be_generated_before_period_end()
			):
				self.subscription.clear_period_state()

			self.subscription.reload()

	def is_trial(self):
		return self.subscription.trial_period_end and getdate(
			self.subscription.trial_period_end
		) >= getdate(nowdate())

	def is_cancelled(self):
		return (
			getdate(self.subscription.cancellation_date) <= getdate(nowdate())
			if self.subscription.cancellation_date
			else False
		)

	def is_billable(self):
		if self.subscription.cancellation_date and getdate(
			self.subscription.cancellation_date
		) < getdate(nowdate()):
			return False

		if self.subscription_state.sales_order:
			if self.order_can_be_generated_before_period_start():
				return True
			return False

		if self.subscription.generate_invoice_at_period_start:
			return True

		elif self.order_can_be_generated_before_period_end():
			return True

		elif self.subscription_state.previous_period_end and getdate(nowdate()) >= getdate(
			self.subscription_state.previous_period_end
		):
			return True

	def order_can_be_generated_before_period_start(self):
		if (
			self.subscription.order_generation_days_before_period
			and self.subscription.generate_invoice_at_period_start
		):
			return self.subscription.current_invoice_end and getdate(nowdate()) >= getdate(
				add_days(
					self.subscription.current_invoice_end,
					cint(self.subscription.order_generation_days_before_period) * -1,
				)
			)

	def order_can_be_generated_before_period_end(self):
		if (
			self.subscription.order_generation_days_before_period
			and not self.subscription.generate_invoice_at_period_start
		):
			return self.subscription_state.previous_period_end and getdate(nowdate()) >= getdate(
				add_days(
					self.subscription_state.previous_period_end,
					cint(self.subscription.order_generation_days_before_period) * -1,
				)
			)

	def is_draft(self):
		if self.sales_invoice:
			return self.sales_invoice.docstatus == 0

	def is_payable(self):
		"""
		A subscription is payable if one of the following condition is fulfilled:
		1. Period Start Invoicing
		- It is linked to an unpaid sales order for the current period
		- It is linked to an unpaid sales invoice for the current period

		2. Period End Invoicing
		- It is linked to an unpaid sales order for the previous period
		- It is linked to an unpaid sales invoice for the previous period
		"""
		if self.is_cancelled() or (not self.sales_order and not self.sales_invoice):
			return False

		if flt(self.sales_order.per_billed) < 100.0:
			if self.sales_order.status == "Closed":
				return False

			if (
				flt(self.sales_order.rounded_total or self.sales_order.grand_total)
				- flt(self.sales_order.advance_paid)
				> 0.0
			):
				return True

		elif self.sales_invoice and flt(self.sales_invoice.outstanding_amount) > 0.0:
			return True

	def is_paid(self):
		if not self.sales_order and not self.sales_invoice:
			return

		if (
			flt(self.sales_order.rounded_total or self.sales_order.grand_total)
			- flt(self.sales_order.advance_paid)
			== 0.0
		) or (self.sales_invoice and flt(self.sales_invoice.outstanding_amount) <= 0.0):
			return True
