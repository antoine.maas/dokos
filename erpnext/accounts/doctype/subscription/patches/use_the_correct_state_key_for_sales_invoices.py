import frappe


def execute():
	"""Use the correct state key for sales invoices"""
	for sub in frappe.get_all("Subscription", fields=["name", "subscription_state"]):
		state = frappe.parse_json(sub.subscription_state) or {}
		if state.get("invoice"):
			doc = frappe.get_doc("Subscription", sub.name)
			doc.set_state("sales_invoice", state["invoice"])
