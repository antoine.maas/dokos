import frappe


def execute():
	frappe.reload_doc("accounts", "doctype", "Mode of Payment")
	fields = ["mode_of_payment", "fee_account", "tax_account", "cost_center", "icon"]
	fields += ["name"]

	for pg in frappe.get_all(
		"Payment Gateway", filters={"mode_of_payment": ("is", "set")}, fields=fields
	):
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "payment_gateway", pg.name)
		frappe.db.set_value("Mode of Payment", pg.mode_of_payment, "icon", pg.icon)

		company = frappe.db.get_value("Account", pg.fee_account, "company")
		mop_account = frappe.db.get_value(
			"Mode of Payment Account", dict(parent=pg.mode_of_payment, company=company)
		)

		frappe.db.set_value("Mode of Payment Account", mop_account, "fee_account", pg.fee_account)
		frappe.db.set_value("Mode of Payment Account", mop_account, "tax_account", pg.tax_account)
		frappe.db.set_value("Mode of Payment Account", mop_account, "cost_center", pg.cost_center)

	for cf in frappe.get_all(
		"Custom Field",
		filters={"is_system_generated": 1, "dt": "Payment Gateway", "fieldname": ("in", fields)},
	):
		frappe.delete_doc("Custom Field", cf.name)
