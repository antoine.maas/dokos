from collections import defaultdict

import frappe
from frappe import _

from erpnext.setup.setup_wizard.operations.install_fixtures import create_recurrence_periods


def execute():
	"""Migrates existing subscriptions and items to the new model"""

	if not frappe.db.exists("DocType", "Subscription Template"):
		return

	frappe.reload_doc("selling", "doctype", "Recurrence Period")

	for dt in ["Subscription Template", "Subscription"]:

		fields = [
			"billing_interval",
			"billing_interval_count",
			"name",
			"generate_invoice_at_period_start",
			"`tabSubscription Plan Detail`.item",
			"`tabSubscription Plan Detail`.booked_item",
			"`tabSubscription Plan Detail`.to_date",
		]
		if dt == "Subscription Template":
			fields.append("start_date")

		subscriptions = frappe.get_all(dt, filters={"billing_interval": ("is", "set")}, fields=fields)
		subscription_recurrences = set(
			(
				s.billing_interval,
				s.billing_interval_count,
				s.generate_invoice_at_period_start,
				(s.get("start_date") if s.get("start_date") != "Creation date" else "Delivery date")
				if dt == "Subscription Template"
				else "Delivery date",
			)
			for s in subscriptions
		)

		for subscription_recurrence in subscription_recurrences:
			title = f"{subscription_recurrence[1]} {_(subscription_recurrence[0])}"
			if frappe.db.exists("Recurrence Period", title):
				title += f" - {frappe.generate_hash(length=5)}"

			if not frappe.db.exists("UOM", subscription_recurrence[0]):
				new_uom = frappe.new_doc("UOM")
				new_uom.uom_name = subscription_recurrence[0]
				new_uom.insert()

			frappe.get_doc(
				{
					"doctype": "Recurrence Period",
					"title": title,
					"billing_interval": subscription_recurrence[0],
					"periodicity": subscription_recurrence[0],
					"billing_interval_count": subscription_recurrence[1],
					"generate_invoice_at_period_start": subscription_recurrence[2],
					"start_date": subscription_recurrence[3],
					"generate_invoice_before_payment": 1,
				}
			).insert(ignore_if_duplicate=True)

		if dt == "Subscription":
			frappe.reload_doc("accounts", "doctype", "Subscription")
			for subscription in subscriptions:
				recurrence = frappe.db.get_value(
					"Recurrence Period",
					dict(
						billing_interval=subscription.billing_interval,
						billing_interval_count=subscription.billing_interval_count,
						generate_invoice_at_period_start=subscription.generate_invoice_at_period_start,
					),
				)

				subscription["recurrence_period"] = recurrence
				frappe.db.set_value(
					dt, subscription.name, "recurrence_period", recurrence, update_modified=False
				)

			migrate_recurring_items(subscriptions)

	if not frappe.get_all("Recurrence Period"):
		for uom in ["Day", "Month", "Year"]:
			if not frappe.db.exists("UOM", uom):
				new_uom = frappe.new_doc("UOM")
				new_uom.uom_name = uom
				new_uom.insert()

		create_recurrence_periods()

	fix_new_orders_status()


def migrate_recurring_items(subscriptions):
	frappe.reload_doc("selling", "doctype", "Item Recurrence Periods")
	frappe.reload_doc("stock", "doctype", "Item")
	items = defaultdict(lambda: defaultdict(set))
	for subscription in subscriptions:
		if not subscription.to_date:
			items[subscription.item]["recurrences"].add(subscription.recurrence_period)
			if subscription.booked_item:
				items[subscription.item]["booked_items"].add(subscription.booked_item)

	for item in items:
		if not items[item].get("booked_items") and not items[item].get("recurrences"):
			continue

		item_doc = frappe.get_doc("Item", item)
		item_doc.is_recurring_item = 1

		if items[item].get("booked_items"):
			if len(items[item]["booked_items"]) > 1:
				print(
					f"Item {item} is linked to the following booked items in current subscriptions: {' ,'.join(items[item]['booked_items'])}.\nSince the model has changed please fix it manually."
				)
			item_doc.booked_item = items[item]["booked_items"][0]
		for recurrence_period in items[item].get("recurrences", []):
			item_doc.append("recurrence_periods", {"recurrence_period": recurrence_period})
		try:
			item_doc.save()
		except Exception:
			print(
				f"Item {item} could not be migrated to a recurring item due to an error.\nPlease do the migration manually."
			)


def fix_new_orders_status():
	for subscription in frappe.get_all("Subscription", filters={"status": ["!=", "Cancelled"]}):
		doc = frappe.get_doc("Subscription", subscription.name)

		if si := frappe.db.get_value(
			"Sales Invoice",
			dict(
				subscription=doc.name, from_date=doc.current_invoice_start, to_date=doc.current_invoice_end
			),
		):
			doc.set_state("sales_invoice", si)

			if pr := frappe.db.get_value(
				"Payment Request",
				dict(reference_doctype="Sales Invoice", reference_name=si),
			):
				doc.set_state("payment_request", pr)

		# Set mode of payment from latest paid payment request
		if prs := frappe.get_all(
			"Payment Request",
			filters=dict(subscription=doc.name, status="Paid"),
			fields=["name", "payment_gateway"],
			order_by="transaction_date DESC",
		):
			if mop := frappe.db.get_value("Mode of Payment", dict(payment_gateway=prs[0].payment_gateway)):
				doc.payment_gateway = mop
				doc.db_set("payment_gateway", mop)

		doc.flags.mute_emails = True
		doc.run_method("process")

		sales_order = doc.get_state_value("sales_order")
		sales_invoice = doc.get_state_value("sales_invoice")

		if (
			sales_order
			and frappe.db.get_value("Sales Order", sales_order, "per_billed") == 0.0
			and sales_invoice
		):
			frappe.db.set_value("Sales Order", sales_order, "per_billed", 100.0)
			frappe.db.set_value("Sales Order", sales_order, "status", "Closed")
