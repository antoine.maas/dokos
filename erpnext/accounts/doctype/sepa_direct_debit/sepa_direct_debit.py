# Copyright (c) 2019, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.query_builder.custom import ConstantColumn
from frappe.utils import cint, flt, getdate, now_datetime, nowdate
from pypika.terms import ExistsCriterion

exclude_from_linked_with = True


class SepaDirectDebit(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.accounts.doctype.sepa_direct_debit_details.sepa_direct_debit_details import (
			SepaDirectDebitDetails,
		)

		amended_from: DF.Link | None
		batch_booking: DF.Check
		company: DF.Link
		currency: DF.Link
		direct_debit_type: DF.Literal["", "One-off", "First", "Recurrent", "Final"]
		from_date: DF.Date | None
		include_generated_entries: DF.Check
		mode_of_payment: DF.Link
		naming_series: DF.Literal["ACC-SEPA-.YYYY.-"]
		payment_entries: DF.Table[SepaDirectDebitDetails]
		payment_order: DF.Link | None
		schema: DF.Data | None
		settings: DF.Link
		to_date: DF.Date | None
		total_amount: DF.Currency
	# end: auto-generated types

	def validate(self):
		self.total_amount = 0
		for entry in self.payment_entries:
			self.total_amount += flt(entry.amount)

	def on_submit(self):
		self.generate_xml_file()

	@frappe.whitelist()
	def get_payment_entries(self):
		if not (self.from_date and self.to_date) and not self.payment_order:
			frappe.throw(
				_(
					"If your SEPA file is not generated from a payment order, From Date and To Date are mandatory"
				)
			)

		payment_entry = frappe.qb.DocType("Payment Entry")
		query = (
			frappe.qb.from_(payment_entry)
			.where(payment_entry.mode_of_payment == self.mode_of_payment)
			.where(payment_entry.docstatus == 1)
			.where(payment_entry.paid_from_account_currency == self.currency)
			.where(payment_entry.reference_date >= self.from_date)
			.where(payment_entry.reference_date <= self.to_date)
			.select(
				ConstantColumn("Payment Entry").as_("payment_document"),
				payment_entry.name.as_("payment_entry"),
				payment_entry.reference_no.as_("cheque_number"),
				payment_entry.reference_date,
				payment_entry.paid_amount.as_("credit"),
				payment_entry.posting_date,
				payment_entry.party.as_("against_account"),
				payment_entry.clearance_date,
			)
		)

		if not self.include_generated_entries:
			sepa_direct_debit_details = frappe.qb.DocType("Sepa Direct Debit Details")
			subquery = (
				frappe.qb.from_(sepa_direct_debit_details)
				.select(sepa_direct_debit_details.name)
				.where(sepa_direct_debit_details.payment_document == "Payment Entry")
				.where(sepa_direct_debit_details.payment_entry == payment_entry.name)
			)
			query = query.where(ExistsCriterion(subquery).negate())

		payment_entries = query.run(as_dict=True)

		entries = sorted(list(payment_entries), key=lambda k: k["reference_date"] or getdate(nowdate()))
		self.set("payment_entries", [])
		self.total_amount = 0.0

		for d in entries:
			row = self.append("payment_entries", {})
			amount = d.credit
			d.amount = amount
			row.update(d)
			self.total_amount += flt(amount)

	@frappe.whitelist()
	def generate_xml_file(self):
		from sepaxml import SepaDD, SepaTransfer

		sepa_settings = frappe.get_doc("Sepa Direct Debit Settings", self.settings)
		company_iban, company_bic = frappe.db.get_value(
			"Bank Account", sepa_settings.bank_account, ["iban", "swift_number"]
		)

		config = {
			"name": sepa_settings.company_name,
			"IBAN": company_iban,
			"BIC": company_bic,
			"batch": self.batch_booking,
			"currency": self.currency,  # ISO 4217
		}

		if sepa_settings.schema.startswith("pain.008"):
			config["creditor_id"] = sepa_settings.creditor_identifier  # supplied by your bank or financial authority
			config["instrument"] = sepa_settings.instrument  # - default is CORE (B2C)
			sepa = SepaDD(config, schema=sepa_settings.schema or "pain.008.001.02", clean=True)
			self.add_dd_payments(sepa, sepa_settings)
		elif sepa_settings.schema.startswith("pain.001"):
			sepa = SepaTransfer(config, schema=sepa_settings.schema or "pain.001.001.03", clean=True)
			self.add_credit_transfer_payments(sepa, sepa_settings)

	def add_dd_payments(self, sepa, sepa_settings):
		for payment_entry in self.payment_entries:
			payment_types = {"One-off": "OOFF", "First": "FRST", "Recurrent": "RCUR", "Final": "FNAL"}
			payment_type = self.direct_debit_type

			customer = payment_entry.against_account
			if not frappe.db.exists(
				"Sepa Mandate", dict(customer=customer, registered_on_gocardless=0, status="Active")
			):
				frappe.throw(_("Please create or activate a SEPA Mandate for customer {0}".format(customer)))

			if (
				len(
					frappe.get_all(
						"Sepa Mandate", dict(customer=customer, registered_on_gocardless=0, status="Active")
					)
				)
				> 1
			):
				frappe.throw(
					_(
						"Customer {0} has several active mandates. Please keep only one active mandate.".format(
							customer
						)
					)
				)

			mandate = frappe.get_doc(
				"Sepa Mandate", dict(customer=customer, registered_on_gocardless=0, status="Active")
			)
			if not mandate.bank_account:
				frappe.throw(
					_("Please add a bank account in mandate {0} for customer {1}".format(mandate.name, customer))
				)

			customer_iban, customer_bic = frappe.db.get_value(
				"Bank Account", mandate.bank_account, ["iban", "swift_number"]
			)

			if not customer_iban:
				frappe.throw(
					_(
						"Please add an IBAN in bank account {0} for customer {1}".format(
							mandate.bank_account, customer
						)
					)
				)

			if not customer_bic:
				frappe.throw(
					_(
						"Please add a Swift Number in bank account {0} for customer {1}".format(
							mandate.bank_account, customer
						)
					)
				)

			pe = frappe.get_doc(payment_entry.payment_document, payment_entry.payment_entry)
			sales_invoices = ""

			for ref in pe.references:
				sales_invoices += "/" + ref.reference_name

			payment_amount = cint(payment_entry.amount * 100)

			payment = {
				"name": customer,
				"IBAN": customer_iban,
				"BIC": customer_bic,
				"amount": payment_amount,  # in cents
				"type": payment_types.get(payment_type),  # FRST,RCUR,OOFF,FNAL
				"collection_date": getdate(payment_entry.reference_date),
				"mandate_id": mandate.mandate,
				"mandate_date": mandate.creation_date,
				"description": sepa_settings.reference_prefix + sales_invoices,
				"endtoend_id": pe.reference_no,  # autogenerated if obmitted
			}
			sepa.add_payment(payment)
		try:
			sepa_export = sepa.export(validate=False)  # TODO: correct false positive upon validation
		except Exception as e:
			frappe.throw(str(e))
		self.save_sepa_export(sepa_export)

		return sepa_export

	def add_credit_transfer_payments(self, sepa, sepa_settings):
		payment_order_date = frappe.db.get_value("Payment Order", self.payment_order, "posting_date")
		for payment_entry in self.payment_entries:
			pe = frappe.get_doc(payment_entry.payment_document, payment_entry.payment_entry)
			if not (
				supplier_bank_account := frappe.db.get_value("Supplier", pe.supplier, "default_bank_account")
			):
				frappe.throw(_("Please add a bank account to supplier {0}").format(pe.supplier))

			bank_account = frappe.get_doc("Bank Account", supplier_bank_account)
			if not bank_account.iban:
				frappe.throw(_("Please add an IBAN to bank account {0}").format(supplier_bank_account))

			payment_amount = cint(payment_entry.amount * 100)

			payment = {
				"name": payment_entry.against_account,
				"IBAN": bank_account.iban,
				"BIC": bank_account.swift_number,
				"amount": payment_amount,  # in cents
				"execution_date": getdate(payment_order_date or payment_entry.reference_date),
				"description": sepa_settings.reference_prefix + " / " + (pe.bill_no or pe.name),
				"endtoend_id": pe.name,  # autogenerated if obmitted
			}
			sepa.add_payment(payment)
		try:
			sepa_export = sepa.export(validate=False)  # TODO: correct false positive upon validation
		except Exception as e:
			frappe.throw(str(e))
		self.save_sepa_export(sepa_export)

		return sepa_export

	def save_sepa_export(self, sepa_export, replace=False):
		_file = frappe.get_doc(
			{
				"doctype": "File",
				"file_name": self.name + "-" + str(now_datetime()) + ".xml",
				"attached_to_doctype": self.doctype,
				"attached_to_name": self.name,
				"is_private": True,
				"content": sepa_export,
			}
		)
		_file.save()

	@frappe.whitelist()
	def create_sepa_payment_entries(self):
		if self.schema.startswith("pain.008"):
			self.create_sepa_direct_debit_payments()
		elif self.schema.startswith("pain.001") and self.payment_order:
			self.create_sepa_credit_transfer_payments_from_payment_order()
		elif self.schema.startswith("pain.001"):
			self.create_sepa_credit_transfer_payments()

	def create_sepa_direct_debit_payments(self):
		from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry

		try:
			open_invoices = frappe.get_all(
				"Sales Invoice",
				filters={
					"status": ["in", ("Unpaid", "Overdue")],
					"due_date": ["between", (self.from_date, self.to_date)],
				},
				fields=["name", "customer", "due_date"],
			)
			for open_invoice in open_invoices:
				if frappe.db.exists(
					"Sepa Mandate",
					dict(customer=open_invoice.customer, status="Active", registered_on_gocardless=0),
				):

					payment_entry = get_payment_entry("Sales Invoice", open_invoice.name)
					payment_entry.mode_of_payment = self.mode_of_payment
					payment_entry.reference_no = (
						open_invoice.customer[:10] + "/" + open_invoice.name
					)  # Must be limited to 35 characters
					payment_entry.reference_date = open_invoice.due_date
					payment_entry.insert()
					payment_entry.submit()
					frappe.db.commit()

			return "Success"
		except Exception:
			payment_entry.log_error(_("SEPA payments generation error"))
			return "Error"

	def create_sepa_credit_transfer_payments_from_payment_order(self):
		payment_order = frappe.get_doc("Payment Order")
		payment_order.run_method("make_payments_in_batch")
		return "Success"

	def create_sepa_credit_transfer_payments(self):
		from erpnext.accounts.doctype.payment_entry.payment_entry import get_payment_entry

		try:
			open_invoices = frappe.get_all(
				"Purchase Invoice",
				filters={
					"status": ["in", ("Unpaid", "Overdue")],
					"due_date": ["between", (self.from_date, self.to_date)],
				},
				fields=["name", "supplier", "due_date"],
			)
			for open_invoice in open_invoices:
				if frappe.get_doc("Supplier").default_payment_method == self.mode_of_payment:
					payment_entry = get_payment_entry("Purchase Invoice", open_invoice.name)
					payment_entry.mode_of_payment = self.mode_of_payment
					payment_entry.reference_no = (
						open_invoice.supplier[:10] + "/" + open_invoice.name
					)  # Must be limited to 35 characters
					payment_entry.reference_date = open_invoice.due_date
					payment_entry.insert()
					payment_entry.submit()
					frappe.db.commit()

			return "Success"
		except Exception:
			payment_entry.log_error(_("SEPA payments generation error"))
			return "Error"
