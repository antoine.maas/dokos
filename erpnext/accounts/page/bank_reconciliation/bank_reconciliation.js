// Copyright (c) 2019, Dokos SAS and Contributors
// For license information, please see license.txt

frappe.provide("erpnext.accounts");
frappe.provide("erpnext.bank_reconciliation");

frappe.pages['bank-reconciliation'].on_page_load = function(wrapper) {
	wrapper.page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __("Bank Reconciliation"),
		single_column: true
	});

	frappe.utils.make_event_emitter(erpnext.bank_reconciliation);

	frappe.require('bank_reconciliation.bundle.js', function() {
		erpnext.accounts.bank_reconciliation_page = new erpnext.accounts.bankReconciliationPage(wrapper);
	});
}
