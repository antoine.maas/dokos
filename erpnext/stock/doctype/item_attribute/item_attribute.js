// Copyright (c) 2019, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.ui.form.on('Item Attribute', {

});


frappe.ui.form.on('Item Attribute Value', {
	attribute_value(frm, cdt, cdn) {
		const row = locals[cdt][cdn];
		if (row.attribute_value && !row.abbr) {
			frappe.model.set_value(row.doctype, row.name, 'abbr', row.attribute_value);
		}
	}
});
