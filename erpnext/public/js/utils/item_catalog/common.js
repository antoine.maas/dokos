const itemDetailsCache = {};

export function fetchItemDetails(item_code, frm, callback) {
	if (itemDetailsCache[item_code]) {
		return callback(itemDetailsCache[item_code]);
	}
	frappe.call({
		method: "erpnext.stock.get_item_details.get_item_details",
		args: {
			args: {
				item_code: item_code,
				set_warehouse: frm.doc.set_warehouse,
				customer: frm.doc.customer || frm.doc.party_name,
				quotation_to: frm.doc.quotation_to,
				supplier: frm.doc.supplier,
				currency: frm.doc.currency,
				conversion_rate: frm.doc.conversion_rate,
				price_list: frm.doc.selling_price_list || frm.doc.buying_price_list,
				price_list_currency: frm.doc.price_list_currency,
				plc_conversion_rate: frm.doc.plc_conversion_rate,
				company: frm.doc.company,
				order_type: frm.doc.order_type,
				is_pos: cint(frm.doc.is_pos),
				is_return: cint(frm.doc.is_return),
				is_subcontracted: frm.doc.is_subcontracted,
				ignore_pricing_rule: frm.doc.ignore_pricing_rule,
				doctype: frm.doc.doctype,
				name: frm.doc.name,
				project: frm.doc.project,
				qty: 1,
				pos_profile: cint(frm.doc.is_pos) ? frm.doc.pos_profile : '',
				is_old_subcontracting_flow: frm.doc.is_old_subcontracting_flow,
			}
		},
		callback(r) {
			if (r.message) {
				itemDetailsCache[item_code] = r.message;
				callback(r.message);
			}
		}
	});
}
