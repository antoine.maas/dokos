import { LinkCatalog } from "frappe/public/js/frappe/ui/catalog";

import CatalogItemGroupTree from "./CatalogItemGroupTree.vue";
import CatalogItemContents from "./CatalogItemContents.vue";

erpnext.ItemCatalogRender = class ItemCatalogRender {
	constructor({ frm }) {
		this.frm = frm;
	}

	get_search_filters() {
		const search_filters = [];
		search_filters.push(["Item", "has_variants", "=", false]);
		search_filters.push(["Item", "disabled", "=", false]);

		const buying_doctypes = ["Purchase Order", "Purchase Invoice", "Purchase Receipt"];
		const selling_doctypes = ["Sales Order", "Sales Invoice", "Sales Receipt"];

		if (selling_doctypes.includes(this.frm.doc.doctype)) {
			search_filters.push(["Item", "is_sales_item", "=", true]);
		} else if (buying_doctypes.includes(this.frm.doc.doctype)) {
			search_filters.push(["Item", "is_purchase_item", "=", true]);
		}

		return search_filters;
	}

	show() {
		if (this.dialog) {
			this.link_catalog?.refresh();
		} else {
			this.make_dialog();
		}

		this.dialog.show();
	}

	make_dialog() {
		this.dialog = new frappe.ui.Dialog({
			size: "extra-large",
			fields: [ { fieldtype: "HTML", fieldname: "catalog" } ],
		});
		const wrapper = this.dialog.body;
		// const wrapper = $(dialog.fields_dict.catalog.$wrapper).get(0);
		this.render(wrapper);
	}

	render(wrapper) {
		const get_search_filters = this.get_search_filters.bind(this);
		this.link_catalog = new LinkCatalog({
			wrapper,
			frm: this.frm,
			options: {
				title: { html: `<h2>${__("Items")}</h2>` },
				link_doctype: "Item",
				search_fields: ["item_name", "item_code"],
				link_fieldname: "item_code",
				quantity_fieldname: "qty",
				table_fieldname: "items",
				sidebar_contents: [
					{ component: CatalogItemGroupTree },
				],
				item_contents: [
					{ component: CatalogItemContents },
				],
				item_footer: [],
				get search_filters() {
					return get_search_filters();
				},
			},
		});
	}
};
