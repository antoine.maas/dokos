// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and Contributors
// MIT License. See license.txt

import "./PMTaskAssignmentQuickEntryForm";
import { PMViewCalendar_Assignments } from "./PMViewCalendar_Assignments";
import { PMViewCalendar_Timelog } from "./PMViewCalendar_Timelog";

/** @typedef {"schedule" | "assign" | "log"} pm_mode_t */
export class ProjectManagementView extends frappe.views.ListView {
	/** @type {pm_mode_t} */ _mode = "log"
	get mode() {
		return this._mode;
	}
	set mode(/** @type {pm_mode_t} */ v) {
		this._mode = v || "log";
		this.setActiveCalendar();
	}

	/** @type {string} */ _date = ""
	get date() {
		const dateObj = this.calendar?.fullcalendar?.getDate() // in UTC, which might be the previous day
		const dateStr = dateObj && frappe.datetime.get_datetime_as_string(dateObj);
		return dateStr?.slice(0,10) ?? this._date;
	}
	set date(v) {
		this._date = v;
		this.calendar?.fullcalendar?.gotoDate(v);
	}

	get_search_params() {
		const params = super.get_search_params()
		params.append("view", this.mode);
		params.append("date", this.date);
		return params;
	}

	parse_filters_from_route_options() {
		if (frappe.route_options) {
			const { view, date, ...rest } = frappe.route_options;
			frappe.route_options = rest;
			this.mode = view ?? "";
			this.date = date ?? "";
		}
		return super.parse_filters_from_route_options();
	}

	static load_last_view() {}
	show_skeleton() {}
	hide_skeleton() {}
	toggle_result_area() {}

	get view_name() {
		return "Project Management";
	}

	setup_defaults() {
		super.setup_defaults();
		this.page_title = __("Project Management");
	}

	setup_view() {
		this.sort_selector.wrapper.hide();
		this.pm_setup_sidebar();
	}

	async refresh() {
		await this.render();
		this.update_url_with_filters();
	}

	process_document_refreshes() {
		this.render();
	}

	get required_libs() {
		return false;
	}

	async render() {
		if (this.calendar) {
			this.calendar.refresh();
			return;
		}

		await this.load_lib;
		await this.setActiveCalendar();
	}

	async setActiveCalendar() {
		for (const [name, btn] of Object.entries(this.buttons)) {
			btn.classList.remove("btn-default", "btn-primary-light");
			if (name === this.mode) {
				btn.classList.add("btn-primary-light");
			} else {
				btn.classList.add("btn-default");
			}
		}

		await this.rebuildCalendar();
	}

	async rebuildCalendar() {
		this.update_url_with_filters();
		this.calendar?.destroy?.();
		this.legendContainer.innerHTML = "";

		// Always hide filters
		this.page.hide_form();
		if (this.mode == "assign" && this.doctype === "Task Assignment") {
			// unless we are in the "assign" mode and the view's doctype is Task Assignment
			this.page.show_form();
		}

		let C = PMViewCalendar_Assignments;
		if (this.mode === "schedule") {
			throw new Error("Not implemented"); // create tasks
		} else if (this.mode === "assign") {
			C = PMViewCalendar_Assignments; // assign task assignments
		} else if (this.mode === "log") {
			C = PMViewCalendar_Timelog; // create timesheets
		}

		this.calendar = new C({
			page: this.page,
			view: this,
			parent: this.$result,
		});
		this.calendar.setupLegend?.(this.legendContainer);
		await this.calendar.make();

		if (this._date) {
			this.calendar.fullcalendar.gotoDate(this._date);
			this.calendar.fullcalendar.on("datesSet", (info) => {
				this._date = info.startStr.slice(0,10);
				this.update_url_with_filters();
			});
		}
	}

	pm_setup_sidebar() {
		if (!this.list_sidebar || this.pmSidebar) return;

		this.list_sidebar.sidebar.html("");
		/** @type {HTMLElement} */
		this.pmSidebar = $(`<div class="taskview-sidebar">`).appendTo(this.list_sidebar.sidebar).get(0);

		this.pm_setup_buttons();
		this.pm_setup_legend();
	}

	pm_setup_buttons() {
		const buttonsContainer = $('<div class="flex flex-column" style="gap:var(--margin-md);"></div>').get(0);
		this.pmSidebar.appendChild(buttonsContainer);

		const makeButton = (mode, label, icon) => {
			const html = `<button class="btn btn-default btn-lg p-2 text-left">
				<span class="float-left mr-2">${icon ? frappe.utils.icon(icon) : ""}</span>
				<span></span>
			</button>`;

			/** @type {HTMLButtonElement} */
			const button = $(html).appendTo(buttonsContainer).get(0);

			const labelEl = button.querySelector("span:last-child");
			labelEl.innerText = label;

			button.addEventListener("click", () => {
				this.mode = mode;
			});

			return button;
		};

		this.buttons = {
			// Schedule Tasks - create projects and tasks
			// schedule: makeButton("schedule"),

			// Assign People - create task assignments
			assign: makeButton("assign", __("Assign", null, "PM Sidebar"), "es-line-add-people"),

			// Log Time - create timesheets
			log: makeButton("log", __("Log Time", null, "PM Sidebar"), "es-line-time"),
		};

		const title = frappe.utils.icon("es-line-add") + "<span>" + __("Add") + "</span>";
		this.page.set_primary_action(title, () => {
			this.calendar.eventSources[0].doGuiCreateDocument();
		});
	}

	pm_setup_legend() {
		/** @type {HTMLElement} */
		this.legendContainer = $('<div class="flex flex-column py-4" style="gap: var(--margin-sm);"></div>').get(0);
		this.legendContainer.classList.add("fc--project-management-view");
		this.pmSidebar.appendChild(this.legendContainer);
	}

	getFilters() {
		return this.filter_area?.get() || []
	}
}

frappe.views.registerCustomListView?.("Project Management", "pm", "es-line-time", ProjectManagementView, (listViewSelect) => {
	return {
		label: "Project Management",
		condition: ["Project", "Task", "Task Assignment", "Timesheet"].includes(listViewSelect.doctype),
		action: () => listViewSelect.set_route("pm"),
		current_view_handler: () => {},
	};
});
