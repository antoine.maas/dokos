# Copyright (c) 2018, Frappe Technologies and contributors
# For license information, please see license.txt

import frappe

from erpnext.regional.report.fichier_des_ecritures_comptables.fichier_des_ecritures_comptables import (
	export_report,
)


# don't remove this function it is used in tests
def test_method():
	"""test function"""
	return "overridden"


def generate_fec_report(doc, method):
	if frappe.db.get_value("Company", doc.company, "country") == "France":
		filters = {"company": doc.company, "fiscal_year": doc.fiscal_year}
		fec_file, title = export_report(filters, return_file=True)

		_file = frappe.get_doc(
			{
				"doctype": "File",
				"file_name": f"{title}.txt",
				"is_private": True,
				"content": fec_file,
				"attached_to_name": doc.name,
				"attached_to_doctype": doc.doctype,
			}
		)
		return _file.insert(ignore_if_duplicate=True)
