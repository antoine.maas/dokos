# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

from collections import defaultdict

import frappe
from frappe import _

from erpnext.accounts.report.financial_statements import get_accounting_entries, get_period_list


def execute(filters=None):
	sig_calculator = SIGCalculator(filters)
	columns = sig_calculator.get_columns()
	data = sig_calculator.get_data()
	summary = sig_calculator.get_report_summary()
	return columns, data, None, None, summary


class SIGCalculator:
	def __init__(self, filters):
		self.filters = filters
		self.revenue_accounts = frappe.get_all(
			"Account", filters={"account_number": ("like", "7%")}, fields=["name", "account_number"]
		)
		self.expense_accounts = frappe.get_all(
			"Account", filters={"account_number": ("like", "6%")}, fields=["name", "account_number"]
		)

		self.labels = [
			_("Ventes de marchandise"),
			_("Achats de marchandises"),
			_("Variation de stock"),
			_("Marge Commerciale"),
			_("Production vendue"),
			_("Production stockée"),
			_("Production immobilisée"),
			_("Production de l'exercice"),
			_("Achats de matières premières et autres approvisionnements"),
			_("Variation des stocks de matières premières"),
			_("Autres achats et charges externes"),
			_("Valeur Ajoutée"),
			_("Subventions d'exploitation"),
			_("Charges de personnel"),
			_("Excédent Brut d'Exploitation (EBE)"),
			_("Dotations aux amortissements, dépréciations et provisions d'exploitation (DADP)"),
			_("Reprises sur amortissements, dépréciations et provisions d'exploitation (RADP)"),
			_("Autres charges"),
			_("Autres produits"),
			_("Résultat d'exploitation"),
			_("Produits financiers"),
			_("Charges financières"),
			_("Résultat courant avant impôts"),
			_("Produits exceptionnels"),
			_("Charges exceptionnelles"),
			_("Résultat exceptionnel"),
			_("Participations des salariés aux résultats"),
			_("Impôts sur les bénéfices"),
			_("Résultat net de l'exercice"),
		]

		self.key_labels = [
			_("Marge Commerciale"),
			_("Production de l'exercice"),
			_("Valeur Ajoutée"),
			_("Excédent Brut d'Exploitation (EBE)"),
			_("Résultat d'exploitation"),
			_("Résultat courant avant impôts"),
			_("Résultat exceptionnel"),
			_("Résultat net de l'exercice"),
		]

		self.period_key = None
		self.period = {}
		self.period_list = get_period_list(
			filters.from_fiscal_year,
			filters.to_fiscal_year,
			filters.period_start_date,
			filters.period_end_date,
			filters.filter_based_on,
			filters.periodicity,
			company=filters.company,
		)

	def get_columns(self):
		columns = [
			{"fieldname": "label", "label": _("Solde"), "fieldtype": "Data", "width": "600px"},
		]

		for period in self.period_list:
			columns.append(
				{
					"fieldname": period.key,
					"label": period.label,
					"fieldtype": "Currency",
					"options": "currency",
					"width": 150,
				}
			)

		return columns

	def get_data(self):
		self.data = defaultdict(dict)

		for period in self.period_list:
			self.period_key = period.key
			self.period = period
			self.calcul_marge_commerciale()
			self.calcul_production()
			self.calcul_valeur_ajoutee()
			self.calcul_ebe()
			self.calcul_resultat_exploitation()
			self.calcul_resultat_avant_impots()
			self.calcul_resultat_exceptionnel()
			self.calcul_resultat_net()

		result = []
		empty_row = False
		for label in self.labels:
			if empty_row:
				result.append({})
				empty_row = False

			row = self.data[label]
			row["label"] = label
			if label in self.key_labels:
				row["label"] = frappe.bold(label)
				row["bold"] = True
				empty_row = True

			result.append(row)

		return result

	def get_accounts(
		self, account_type: str, selected_numbers: tuple | str, excluded_numbers: list = None
	):
		accounts_list = self.revenue_accounts if account_type == "revenue" else self.expense_accounts

		if excluded_numbers:
			return [
				a.name
				for a in accounts_list
				if a.account_number.startswith(selected_numbers)
				and not a.account_number.startswith(excluded_numbers)
			]
		else:
			return [a.name for a in accounts_list if a.account_number.startswith(selected_numbers)]

	def get_accounting_entries(self, accounts):
		return get_accounting_entries(
			"GL Entry",
			self.period.from_date,
			self.period.to_date,
			accounts,
			self.filters,
			True,
		)

	def get_accounts_balance(self, accounts):
		if not accounts:
			return 0.0

		entries = self.get_accounting_entries(accounts)

		return sum(entry.credit - entry.debit for entry in entries)

	def calcul_marge_commerciale(self):
		# 1. Marge commerciale

		# 1.1 Ventes de marchandises
		revenue_accounts = self.get_accounts("revenue", "707")
		self.total_revenue = self.get_accounts_balance(revenue_accounts)
		self.data[_("Ventes de marchandise")][self.period_key] = self.total_revenue

		# 1.2 Achats de marchandises
		cogs_accounts = self.get_accounts("expense", "607")
		total_cogs_expenses = self.get_accounts_balance(cogs_accounts)
		self.data[_("Achats de marchandises")][self.period_key] = total_cogs_expenses

		# 1.2 Variations des stock de marchandises
		stock_variation_accounts = self.get_accounts("expense", "6037")
		total_stock_variation = self.get_accounts_balance(stock_variation_accounts)
		self.data[_("Variation de stock")][self.period_key] = total_stock_variation

		self.sales_margin = self.total_revenue + total_cogs_expenses + total_stock_variation
		self.data[_("Marge Commerciale")][self.period_key] = self.sales_margin

	def calcul_production(self):
		# 2. Production de l'exercice
		# 2.1 Production vendue
		sold_production_accounts = self.get_accounts("revenue", "70", ("707", "7087", "7097"))
		sold_production = self.get_accounts_balance(sold_production_accounts)
		self.data[_("Production vendue")][self.period_key] = sold_production

		# 2.2 Production stockée
		stocked_production_accounts = self.get_accounts("revenue", "71")
		stocked_production = self.get_accounts_balance(stocked_production_accounts)
		self.data[_("Production stockée")][self.period_key] = stocked_production

		# 2.2 Production immobilisée
		capitalised_production_accounts = self.get_accounts("revenue", "72")
		capitalised_production = self.get_accounts_balance(capitalised_production_accounts)
		self.data[_("Production immobilisée")][self.period_key] = capitalised_production

		self.total_production = sold_production - stocked_production - capitalised_production
		self.data[_("Production de l'exercice")][self.period_key] = self.total_production

	def calcul_valeur_ajoutee(self):
		# 3. Valeur Ajoutée
		# 3.1 Achats de matières premières et autres approvisionnements
		expense_accounts = self.get_accounts("expense", "60", ("607", "603"))
		total_expenses = self.get_accounts_balance(expense_accounts)
		self.data[_("Achats de matières premières et autres approvisionnements")][
			self.period_key
		] = total_expenses

		# 3.2 Variation des stocks de matières premières
		stock_variation_accounts = self.get_accounts("expense", "603", "6037")
		total_stock_variations = self.get_accounts_balance(stock_variation_accounts)
		self.data[_("Variation des stocks de matières premières")][
			self.period_key
		] = total_stock_variations

		# 3.3 Autres achats et charges externes
		external_expenses_accounts = self.get_accounts("expense", ("61", "62"))
		total_external_expenses = self.get_accounts_balance(external_expenses_accounts)
		self.data[_("Autres achats et charges externes")][self.period_key] = total_external_expenses

		self.total_value_added = (
			self.sales_margin
			+ self.total_production
			+ total_expenses
			+ total_stock_variations
			+ total_external_expenses
		)
		self.data[_("Valeur Ajoutée")][self.period_key] = self.total_value_added

	def calcul_ebe(self):
		# 4. Excédent Brut d'Exploitation (EBE)
		# 4.1 Subventions d'exploitations
		subsidy_accounts = self.get_accounts("revenue", "74")
		subsidies = self.get_accounts_balance(subsidy_accounts)
		self.data[_("Subventions d'exploitation")][self.period_key] = subsidies

		# 4.2 Charges de personnel
		employees_accounts = self.get_accounts("expense", ("63", "64"))
		employees_expenses = self.get_accounts_balance(employees_accounts)
		self.data[_("Charges de personnel")][self.period_key] = employees_expenses

		self.ebe = self.total_value_added + subsidies + employees_expenses
		self.data[_("Excédent Brut d'Exploitation (EBE)")][self.period_key] = self.ebe

	def calcul_resultat_exploitation(self):
		# 5. Résultat d'exploitation
		# 5.1 Dotations aux amortissements, dépréciations et provisions d'exploitation (DADP)
		dadp_accounts = self.get_accounts("expense", "68")
		dadp = self.get_accounts_balance(dadp_accounts)
		self.data[_("Dotations aux amortissements, dépréciations et provisions d'exploitation (DADP)")][
			self.period_key
		] = dadp

		# 5.2 Reprises sur amortissements, dépréciations et provisions d'exploitation (RADP)
		radp_accounts = self.get_accounts("revenue", "78")
		radp = self.get_accounts_balance(radp_accounts)
		self.data[_("Reprises sur amortissements, dépréciations et provisions d'exploitation (RADP)")][
			self.period_key
		] = radp

		# 5.3 Autres charges
		other_expenses_accounts = self.get_accounts("expense", "65")
		other_expenses = self.get_accounts_balance(other_expenses_accounts)
		self.data[_("Autres charges")][self.period_key] = other_expenses

		# 5.4 Autres produits
		other_revenue_accounts = self.get_accounts("revenue", "75")
		other_revenue = self.get_accounts_balance(other_revenue_accounts)
		self.data[_("Autres produits")][self.period_key] = other_revenue

		self.ebit = self.ebe + dadp + radp + other_expenses + other_revenue
		self.data[_("Résultat d'exploitation")][self.period_key] = self.ebit

	def calcul_resultat_avant_impots(self):
		# 6. Résultat courant avant impôts
		# 6.1 Produits financiers
		financial_revenue_accounts = self.get_accounts("revenue", "76")
		financial_revenue = self.get_accounts_balance(financial_revenue_accounts)
		self.data[_("Produits financiers")][self.period_key] = financial_revenue

		# 4.2 Charges financières
		financial_expenses_accounts = self.get_accounts("expense", "66")
		financial_expenses = self.get_accounts_balance(financial_expenses_accounts)
		self.data[_("Charges financières")][self.period_key] = financial_expenses

		self.resultat_avant_impots = self.ebit + financial_revenue + financial_expenses
		self.data[_("Résultat courant avant impôts")][self.period_key] = self.resultat_avant_impots

	def calcul_resultat_exceptionnel(self):
		# 7. Résultat exceptionnel
		# 7.1 Produits exceptionnels
		exceptional_revenue_accounts = self.get_accounts("revenue", "77")
		exceptional_revenue = self.get_accounts_balance(exceptional_revenue_accounts)
		self.data[_("Produits exceptionnels")][self.period_key] = exceptional_revenue

		# 7.2 Charges exceptionnelles
		exceptional_expenses_accounts = self.get_accounts("expense", "67")
		exceptional_expenses = self.get_accounts_balance(exceptional_expenses_accounts)
		self.data[_("Charges exceptionnelles")][self.period_key] = exceptional_expenses

		self.resultat_exceptionnel = exceptional_revenue + exceptional_expenses
		self.data[_("Résultat exceptionnel")][self.period_key] = self.resultat_exceptionnel

	def calcul_resultat_net(self):
		# 8. Résultat net de l'exercice
		# 8.1 Participations des salariés aux résultats
		participation_accounts = self.get_accounts("expense", "691")
		participation = self.get_accounts_balance(participation_accounts)
		self.data[_("Participations des salariés aux résultats")][self.period_key] = participation

		# 8.2 Impôts sur les bénéfices
		taxes_accounts = self.get_accounts("expense", "695")
		total_taxes = self.get_accounts_balance(taxes_accounts)
		self.data[_("Impôts sur les bénéfices")][self.period_key] = total_taxes

		self.resultat_net = (
			self.resultat_avant_impots + self.resultat_exceptionnel + participation + total_taxes
		)
		self.data[_("Résultat net de l'exercice")][self.period_key] = self.resultat_net

	def get_report_summary(self):
		summary = []

		if self.total_revenue:
			summary.append(
				{
					"value": self.sales_margin / self.total_revenue * 100,
					"label": _("Taux de marge commerciale"),
					"datatype": "Percent",
					"indicator": "Blue",
				},
			)

		return summary
