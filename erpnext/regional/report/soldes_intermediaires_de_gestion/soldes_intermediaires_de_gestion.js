// Copyright (c) 2023, Dokos SAS and contributors
// For license information, please see license.txt

frappe.query_reports["Soldes Intermediaires de Gestion"] = $.extend(
	{},
	erpnext.financial_statements
);

erpnext.utils.add_dimensions("Soldes Intermediaires de Gestion", 10);

frappe.query_reports["Soldes Intermediaires de Gestion"]["filters"].splice(8, 1)
frappe.query_reports["Soldes Intermediaires de Gestion"]["filters"].splice(1, 1)

Object.assign(frappe.query_reports["Soldes Intermediaires de Gestion"], {
	"formatter": function(value, row, column, data, default_formatter, filter) {
		if (row[1].content && column.fieldtype == "Currency") {
			value = default_formatter(value, row, column, data);
		}
		return value || "";
	}
});