// Copyright (c) 2018, Frappe Technologies Pvt. Ltd. and contributors
// For license information, please see license.txt

frappe.query_reports["Fichier des Ecritures Comptables"] = {
	"filters": [
		{
			"fieldname": "company",
			"label": __("Company"),
			"fieldtype": "Link",
			"options": "Company",
			"default": frappe.defaults.get_user_default("Company"),
			"reqd": 1
		},
		{
			"fieldname": "fiscal_year",
			"label": __("Fiscal Year"),
			"fieldtype": "Link",
			"options": "Fiscal Year",
			"default": erpnext.utils.get_fiscal_year(frappe.datetime.get_today()),
			"reqd": 1,
			on_change: function() {
				const fiscal_year = frappe.query_report.get_filter_value('fiscal_year');
				const fiscal_year_dates = erpnext.utils.get_fiscal_year(fiscal_year, true)
				frappe.query_report.set_filter_value('from_date', fiscal_year_dates[1]);
				frappe.query_report.set_filter_value('to_date', fiscal_year_dates[2]);
			}
		},
		{
			"fieldname": "from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": erpnext.utils.get_fiscal_year(frappe.datetime.get_today(), true)[1],
			"reqd": 1
		},
		{
			"fieldname": "to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": erpnext.utils.get_fiscal_year(frappe.datetime.get_today(), true)[2],
			"reqd": 1
		}
	],
	onload: function(query_report) {
		query_report.page.add_inner_button(__("Export"), function() {
			fec_export(query_report);
		});

		query_report.page.add_inner_button(__("Export with reference files"), function() {
			fec_export(query_report, true);
		});

		query_report.add_chart_buttons_to_toolbar = function() {
			//
		};

		query_report.add_card_button_to_toolbar = function() {
			//
		};

		query_report.export_report = function() {
			fec_export(query_report);
		};
	}
};

const fec_export = function(query_report, with_files=false) {
	frappe.show_alert({
		message: __("Your FEC is being prepared"),
		indicator: "green",
	})

	if (with_files) {
		frappe.call({
			method: "erpnext.regional.report.fichier_des_ecritures_comptables.fichier_des_ecritures_comptables.export_report",
			args: {
				filters: query_report.get_values(),
				with_files: with_files
			}
		}).then(r => {
			if (!r.exc) {
				frappe.show_alert({
					message: __("Report generation in progress.<br> You will receive an email with a link to download the zip file in a few minutes."),
					indicator: "orange"
				})
			}
		})
	} else {
		open_url_post(frappe.request.url, {
			cmd: "erpnext.regional.report.fichier_des_ecritures_comptables.fichier_des_ecritures_comptables.export_report",
			filters: query_report.get_values()
		})
	}
};