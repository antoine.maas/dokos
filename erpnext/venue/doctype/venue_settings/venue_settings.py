# Copyright (c) 2020, Dokos SAS and contributors
# For license information, please see license.txt
from dataclasses import dataclass
from typing import Literal

import frappe
from frappe import _
from frappe.model.document import Document
from frappe.utils import cint


@dataclass
class venue_uom_info_t:
	selector: Literal["long", "short"]
	target_type: Literal["Minute", "Day", "Week", "Month", "Year"]
	value: int | float
	from_uom: str
	to_uom: str


class VenueSettings(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.venue.doctype.venue_cart_settings.venue_cart_settings import VenueCartSettings
		from erpnext.venue.doctype.venue_units_of_measure.venue_units_of_measure import (
			VenueUnitsofMeasure,
		)
		from erpnext.venue.doctype.venue_uom_conversion.venue_uom_conversion import VenueUOMConversion

		allow_event_cancellation: DF.Check
		cancellation_delay: DF.Duration | None
		cart_settings_overrides: DF.Table[VenueCartSettings]
		clear_item_booking_draft_duration: DF.Int
		confirm_booking_after_payment: DF.Check
		day_uom: DF.Link | None
		enable_multi_companies: DF.Check
		enable_simultaneous_booking: DF.Check
		minute_uom: DF.Link | None
		month_uom: DF.Link | None
		no_overlap_per_item: DF.Check
		registration_item_code: DF.Link | None
		role_allowed_to_skip_cart: DF.Link | None
		sync_with_google_calendar: DF.Check
		venue_long_uoms: DF.Table[VenueUOMConversion]
		venue_units_of_measure: DF.Table[VenueUnitsofMeasure]
		week_uom: DF.Link | None
	# end: auto-generated types

	# Support for multiple companies/venues with distinct items
	def onload(self):
		# see: webshop_settings.py
		self.get("__onload").quotation_series = frappe.get_meta("Quotation").get_options("naming_series")

	def configure_uom_conversions(self):
		if self.minute_uom:
			for row in self.venue_units_of_measure:
				self.venue_upsert_uom_conversion(row.unit_of_measure, self.minute_uom, cint(row.duration) / 60)

		self.venue_configure_long_uom_conversions()

	def venue_upsert_uom_conversion(self, from_uom: str, to_uom: str, value: int | float):
		if not from_uom or not to_uom or value <= 0:
			return

		conversion = frappe.db.get_value(
			"UOM Conversion Factor",
			filters={"from_uom": from_uom, "to_uom": to_uom},
			fieldname=["name", "value"],
			as_dict=True,
			for_update=True,
		)
		if conversion and conversion.value == value:
			return

		if conversion:
			conversion = frappe.get_doc("UOM Conversion Factor", conversion)
		else:
			conversion = frappe.new_doc("UOM Conversion Factor")

		conversion.category = self.venue_make_time_category()
		conversion.from_uom = from_uom
		conversion.to_uom = to_uom
		conversion.value = value
		conversion.save(ignore_permissions=True)

	def get_uom_aliases(self):
		return {
			"Minute": self.minute_uom,
			"Day": self.day_uom,
			"Week": self.week_uom,
			"Month": self.month_uom,
			"Year": None,
		}

	def venue_configure_long_uom_conversions(self):
		uom_aliases = self.get_uom_aliases()
		for row in self.venue_long_uoms:
			to_uom = uom_aliases[row.target_type]
			if to_uom:
				self.venue_upsert_uom_conversion(row.from_uom, to_uom, row.value)

	def venue_make_time_category(self):
		category = frappe.db.exists("UOM Category", "Time")
		if not category:
			category = frappe.db.exists("UOM Category", _("Time"))

		if not category:
			category_doc = frappe.new_doc("UOM Category")
			category_doc.category_name = _("Time")
			category_doc.insert(ignore_permissions=True)
			category = category_doc.name

		return category

	def get_uom_infos(self) -> dict[str, venue_uom_info_t]:
		uom_aliases = self.get_uom_aliases()
		uom_infos = {}
		for long_uom in self.venue_long_uoms:
			base_uom = uom_aliases[long_uom.target_type]
			if not base_uom:
				continue
			uom_infos[long_uom.from_uom] = venue_uom_info_t(
				selector="long",
				target_type=long_uom.target_type,
				value=long_uom.value,
				from_uom=long_uom.from_uom,
				to_uom=base_uom,
			)
		for target_type in ("Month", "Week", "Day", "Year"):
			if uom := getattr(self, f"{target_type.lower()}_uom", None):
				uom_infos[uom] = venue_uom_info_t(
					selector="long",
					target_type=target_type,
					value=1,
					from_uom=uom,
					to_uom=uom,
				)
		if minute_uom := self.minute_uom:
			uom_infos[minute_uom] = venue_uom_info_t(
				selector="short",
				target_type="Minute",
				value=1,
				from_uom=minute_uom,
				to_uom=minute_uom,
			)
			# conversions = frappe.get_all(
			# 	"UOM Conversion Factor",
			# 	filters={"to_uom": minute_uom},
			# 	fields=["from_uom", "to_uom", "value"],
			# 	order_by="modified desc",
			# )
			# for conv in conversions:
			# 	if conv.from_uom not in uom_infos:
			# 		uom_infos[conv.from_uom] = venue_uom_info_t(
			# 			selector="short",
			# 			target_type="Minute",
			# 			value=conv.value,
			# 			from_uom=conv.from_uom,
			# 			to_uom=minute_uom,
			# 		)
		return uom_infos


@frappe.whitelist()
def create_role_profile_fields():
	from frappe.custom.doctype.custom_field.custom_field import create_custom_fields

	custom_fields = {}
	for dt, insert_after in {
		"Customer": "customer_primary_contact",
		"Subscription": "contact_person",
	}.items():
		df = dict(
			doctype=dt,
			fieldname="role_profile_name",
			label="Role Profile",
			fieldtype="Link",
			insert_after=insert_after,
			options="Role Profile",
			description="All users associated with this customer will be attributed this role profile",
		)
		custom_fields[dt] = [df]

	create_custom_fields(custom_fields)


@frappe.whitelist()
def get_duration_for_uom(uom, minute_uom):
	from erpnext.venue.doctype.item_booking.item_booking import get_uom_in_minutes

	return get_uom_in_minutes(uom, minute_uom) * 60


@frappe.whitelist()
def simultaneous_booking_enabled():
	return bool(frappe.db.get_single_value("Venue Settings", "enable_simultaneous_booking"))


@frappe.whitelist()
def get_booking_uoms(*args):
	valid_uoms: list[str] = []

	venue_settings = frappe.get_single("Venue Settings")
	for fieldname in ["minute_uom", "day_uom", "week_uom", "month_uom"]:
		if uom := venue_settings.get(fieldname):
			valid_uoms.append(uom)

	all_uoms = valid_uoms + frappe.get_all(
		"UOM Conversion Factor", filters={"to_uom": ("in", valid_uoms)}, pluck="from_uom"
	)

	out = []
	seen = set()
	for uom in all_uoms:
		print(uom)
		if uom not in seen:
			out.append([uom])
			seen.add(uom)

	out.sort()
	return out
