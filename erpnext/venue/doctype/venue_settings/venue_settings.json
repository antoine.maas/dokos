{
 "actions": [],
 "creation": "2020-08-03 08:10:42.541814",
 "doctype": "DocType",
 "editable_grid": 1,
 "engine": "InnoDB",
 "field_order": [
  "item_booking_section",
  "clear_item_booking_draft_duration",
  "confirm_booking_after_payment",
  "column_break_4",
  "enable_simultaneous_booking",
  "no_overlap_per_item",
  "sync_with_google_calendar",
  "allow_event_cancellation",
  "cancellation_delay",
  "role_allowed_to_skip_cart",
  "event_registration_section",
  "registration_item_code",
  "multi_venue_section",
  "enable_multi_companies",
  "cart_settings_overrides",
  "unit_of_measure_tab",
  "short_bookings_section",
  "minute_uom",
  "venue_units_of_measure",
  "section_break_20",
  "month_uom",
  "column_break_uuvm",
  "week_uom",
  "column_break_jmrs",
  "day_uom",
  "section_break_lnrr",
  "venue_long_uoms"
 ],
 "fields": [
  {
   "fieldname": "item_booking_section",
   "fieldtype": "Section Break",
   "label": "Item Booking"
  },
  {
   "description": "Used for slots calculation.<br>Please add an UOM conversion factor between each UOM used in booked items and this UOM.",
   "fieldname": "minute_uom",
   "fieldtype": "Link",
   "label": "Minute UOM",
   "mandatory_depends_on": "eval:doc.venue_units_of_measure?.length",
   "options": "UOM"
  },
  {
   "default": "15",
   "description": "Minimum 3 minutes.<br>Set 0 to disable this functionality.",
   "fieldname": "clear_item_booking_draft_duration",
   "fieldtype": "Int",
   "label": "Clear drafts after x minutes"
  },
  {
   "fieldname": "column_break_4",
   "fieldtype": "Column Break"
  },
  {
   "default": "0",
   "fieldname": "enable_simultaneous_booking",
   "fieldtype": "Check",
   "label": "Enable simultaneous booking"
  },
  {
   "default": "0",
   "description": "Applicable for shopping cart bookings",
   "fieldname": "sync_with_google_calendar",
   "fieldtype": "Check",
   "label": "Automatically synchronize with Google Calendar"
  },
  {
   "fieldname": "month_uom",
   "fieldtype": "Link",
   "label": "Month UOM",
   "options": "UOM"
  },
  {
   "default": "0",
   "fieldname": "no_overlap_per_item",
   "fieldtype": "Check",
   "label": "Do not allow overlapping bookings for the same item on desk"
  },
  {
   "default": "0",
   "fieldname": "allow_event_cancellation",
   "fieldtype": "Check",
   "label": "Allow cancellation of item bookings on the portal"
  },
  {
   "default": "86400",
   "depends_on": "eval:doc.allow_event_cancellation",
   "description": "Users will not be able to cancel later than this delay before the appointment",
   "fieldname": "cancellation_delay",
   "fieldtype": "Duration",
   "label": "Cancellation delay"
  },
  {
   "default": "0",
   "description": "If not set, the booking will be considered confirmed when the order is placed",
   "fieldname": "confirm_booking_after_payment",
   "fieldtype": "Check",
   "label": "Confirm booking after payment"
  },
  {
   "fieldname": "event_registration_section",
   "fieldtype": "Section Break",
   "label": "Event Registration"
  },
  {
   "fieldname": "registration_item_code",
   "fieldtype": "Link",
   "label": "Billed Item for registration",
   "options": "Item"
  },
  {
   "default": "0",
   "fieldname": "enable_multi_companies",
   "fieldtype": "Check",
   "label": "Enable"
  },
  {
   "depends_on": "enable_multi_companies",
   "fieldname": "cart_settings_overrides",
   "fieldtype": "Table",
   "label": "Allowed companies",
   "mandatory_depends_on": "enable_multi_companies",
   "options": "Venue Cart Settings"
  },
  {
   "fieldname": "multi_venue_section",
   "fieldtype": "Tab Break",
   "label": "Multi-venue mode"
  },
  {
   "description": "Users with this role will be able to make bookings without having to validate a shopping cart and create a sales order",
   "fieldname": "role_allowed_to_skip_cart",
   "fieldtype": "Link",
   "label": "Role allowed to skip shopping cart",
   "options": "Role"
  },
  {
   "fieldname": "unit_of_measure_tab",
   "fieldtype": "Tab Break",
   "label": "Units of Measure"
  },
  {
   "description": "Example: For the unit Half-Day set a duration of 4 hours.",
   "fieldname": "venue_units_of_measure",
   "fieldtype": "Table",
   "label": "Conversion Table",
   "options": "Venue Units of Measure"
  },
  {
   "fieldname": "section_break_20",
   "fieldtype": "Section Break",
   "hide_border": 1,
   "label": "Long Bookings"
  },
  {
   "fieldname": "short_bookings_section",
   "fieldtype": "Section Break",
   "label": "Short Bookings"
  },
  {
   "fieldname": "week_uom",
   "fieldtype": "Link",
   "label": "Week UOM",
   "options": "UOM"
  },
  {
   "fieldname": "column_break_uuvm",
   "fieldtype": "Column Break"
  },
  {
   "fieldname": "section_break_lnrr",
   "fieldtype": "Section Break"
  },
  {
   "description": "Example: For the unit '3 month' set a quantity of 3 and a target unit of Month.",
   "fieldname": "venue_long_uoms",
   "fieldtype": "Table",
   "label": "Conversion Table",
   "options": "Venue UOM Conversion"
  },
  {
   "fieldname": "column_break_jmrs",
   "fieldtype": "Column Break"
  },
  {
   "fieldname": "day_uom",
   "fieldtype": "Link",
   "label": "Day UOM",
   "options": "UOM"
  }
 ],
 "issingle": 1,
 "links": [],
 "modified": "2024-02-28 15:25:05.173966",
 "modified_by": "Administrator",
 "module": "Venue",
 "name": "Venue Settings",
 "owner": "Administrator",
 "permissions": [
  {
   "create": 1,
   "delete": 1,
   "email": 1,
   "print": 1,
   "read": 1,
   "role": "System Manager",
   "share": 1,
   "write": 1
  },
  {
   "create": 1,
   "delete": 1,
   "email": 1,
   "print": 1,
   "read": 1,
   "role": "Venue Manager",
   "share": 1,
   "write": 1
  }
 ],
 "sort_field": "modified",
 "sort_order": "DESC",
 "states": []
}
