# Copyright (c) 2023, Dokos SAS and Contributors
# See license.txt

from collections import Counter
from contextlib import contextmanager
from datetime import date, datetime
from operator import itemgetter

import frappe
from frappe.tests.utils import FrappeTestCase, change_settings
from frappe.utils import add_to_date, get_datetime, getdate

from erpnext import get_default_company
from erpnext.venue.doctype.item_booking.item_booking import get_availabilities

TEST_CUSTOMER = "_Test Customer 1"
TEST_USER_1 = "test@example.com"
TEST_USER_2 = "test2@example.com"
ALL_DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

MAX_SIMULTANEOUS_BOOKINGS_1 = 2


class BaseTestWithBookableItem(FrappeTestCase):
	@classmethod
	def setUpClass(cls) -> None:
		super().setUpClass()

		cls._old_curr_date = frappe.flags.current_date
		frappe.flags.current_date = None

		cls.ITEM_BOOKABLE_1 = frappe.get_doc(
			{
				"doctype": "Item",
				"item_code": "Coworking space - _Test Item for Item Booking with Subscription",
				"item_group": frappe.get_value("Item Group", [("parent_item_group", "=", "")]),
				"sales_uom": "Hour",
				"is_stock_item": 0,
				"enable_item_booking": 1,
				"simultaneous_bookings_allowed": MAX_SIMULTANEOUS_BOOKINGS_1,
			}
		)
		cls.ITEM_BOOKABLE_1.insert()

		cls.ITEM_SUB_1 = frappe.get_doc(
			{
				"doctype": "Item",
				"item_code": "Monthly subscription coworking - _Test Item for Item Booking with Subscription",
				"item_group": frappe.get_value("Item Group", [("parent_item_group", "=", "")]),
				"is_stock_item": 0,
				"sales_uom": "Unit",
				"booked_item": cls.ITEM_BOOKABLE_1.name,
				"is_recurring_item": 1,
				"recurrence_periods": [{"recurrence_period": "Monthly"}],
			}
		)
		cls.ITEM_SUB_1.insert()

		# cls.ITEM_TICKET_1 = frappe.get_doc({
		# 	"doctype": "Item",
		# 	"item_code": "Coworking ticket - _Test Item for Item Booking with Subscription",
		# 	"item_group": frappe.get_value("Item Group", [("parent_item_group", "=", "")]),
		# 	"sales_uom": "Unit",
		# })
		# cls.ITEM_TICKET_1.insert()

		cls.CALENDAR_1 = frappe.get_doc(
			{
				# This is the actual time slots available for your test item
				"doctype": "Item Booking Calendar",
				"calendar_title": "_Test Booking Calendar for Item Booking with Subscription",
				"uom": None,
				"item": cls.ITEM_BOOKABLE_1.name,
				"booking_calendar": [
					{"day": d, "start_time": "08:00:00", "end_time": "18:00:00"} for d in ALL_DAYS
				],
			}
		)
		cls.CALENDAR_1.insert()

		cls.ITEM_BOOKABLE_2 = frappe.get_doc(
			{
				"doctype": "Item",
				"item_code": "Rarely available coworking space",
				"item_group": frappe.get_value("Item Group", [("parent_item_group", "=", "")]),
				"sales_uom": "Hour",
				"is_stock_item": 0,
				"enable_item_booking": 1,
				"simultaneous_bookings_allowed": 1,
			}
		)
		cls.ITEM_BOOKABLE_2.insert()

		cls.CALENDAR_2 = frappe.get_doc(
			{
				# This is the actual time slots available for your test item
				"doctype": "Item Booking Calendar",
				"calendar_title": "_Test Booking Calendar for Rarely available coworking space",
				"uom": None,
				"item": cls.ITEM_BOOKABLE_2.name,
				"booking_calendar": [
					{"day": d, "start_time": "08:00:00", "end_time": "18:00:00"} for d in ALL_DAYS
				],
				"exceptions": [
					{
						"type": "Remove",
						"start_date": "2900-01-01",
						"end_date": "3000-12-31",
						"start_time": "",
						"end_time": "",
					},
					{
						"type": "Add",
						"start_date": "2901-01-01",
						"end_date": "2901-01-01",
						"start_time": "10:00:00",
						"end_time": "12:00:00",
					},
					{
						"type": "Add",
						"start_date": "2901-01-02",
						"end_date": "2901-01-02",
						"start_time": "",
						"end_time": "",
					},
				],
			}
		)
		cls.CALENDAR_2.insert()

		cls.ITEM_BOOKABLE_3 = frappe.get_doc(
			{
				"doctype": "Item",
				"item_code": "Coworking space with whole slots",
				"item_group": frappe.get_value("Item Group", [("parent_item_group", "=", "")]),
				"sales_uom": "Hour",
				"is_stock_item": 0,
				"enable_item_booking": 1,
				"simultaneous_bookings_allowed": 1,
			}
		)
		cls.ITEM_BOOKABLE_3.insert()

		cls.CALENDAR_3 = frappe.get_doc(
			{
				# This is the actual time slots available for your test item
				"doctype": "Item Booking Calendar",
				"calendar_title": "_Test Booking Calendar for Item Booking with Whole Slots",
				"uom": None,
				"item": cls.ITEM_BOOKABLE_3.name,
				"booking_calendar": [
					{"day": d, "start_time": "08:00:00", "end_time": "18:00:00", "whole": True} for d in ALL_DAYS
				],
			}
		)
		cls.CALENDAR_3.insert()

	@classmethod
	def tearDownClass(cls) -> None:
		frappe.flags.current_date = cls._old_curr_date
		return super().tearDownClass()

	def makeBookingWithAutocleanup(self, *args, **kwargs):
		booking = self.makeBooking(*args, **kwargs)
		self.addCleanup(booking.delete)
		return booking

	def makeBooking(
		self,
		booked_item: str,
		start: datetime,
		end: datetime,
		user: str | None = None,
		all_day=False,
		uom="Hour",
		status="Confirmed",
	):
		booking = frappe.get_doc(
			{
				"doctype": "Item Booking",
				"item": booked_item,
				"starts_on": start,
				"ends_on": end,
				"user": user,
				"status": status,
				"all_day": all_day,
				"uom": uom,
				"sync_with_google_calendar": False,
			}
		)
		booking.insert()
		return booking


class BaseTestWithSubscriptionForBookableItem(BaseTestWithBookableItem):
	@classmethod
	def makeSubscription(
		self,
		start_date,
		*,
		customer: str = TEST_CUSTOMER,
		item: dict = None,
		company: str = get_default_company(),  # noqa: B008
		item_booking: bool = True
	):
		if isinstance(start_date, date):
			pass  # ok
		elif isinstance(start_date, datetime):
			start_date = start_date.date()  # convert for convenience
		elif len(str(start_date)) != 10:
			raise ValueError(
				"testing: makeSubscription(...) start_date parameter must be a `date` object, got a "
				+ repr(type(start_date))
			)

		subscription = frappe.get_doc(
			doctype="Subscription",
			company=company,
			customer=customer,
			start=getdate(start_date),
			recurrence_period="Monthly",
			plans=[
				{
					"item": item,
					"qty": 1,
					"uom": "Month",
					"booked_item": self.ITEM_BOOKABLE_1.name if item_booking else None,
				}
			],
		)
		subscription.insert(ignore_permissions=True)
		return subscription


class TestItemBooking(BaseTestWithBookableItem):
	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 1, "no_overlap_per_item": 1},
	)
	def test_booking1(self):
		dt_start = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_end = add_to_date(dt_start, hours=1)  # 9:00
		self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)  # Create a booking

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 1},
	)
	def test_booking2(self):  # identical, but without simultaneous bookings
		dt_start = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_end = add_to_date(dt_start, hours=1)  # 9:00
		self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 1},
	)
	def test_booking_overlap_exact_same_dt(self):
		dt_start = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_end = add_to_date(dt_start, hours=1)  # 9:00
		self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)

		with self.assertRaises(frappe.ValidationError):
			# Here we try to book on the exact same slot: this should raise an exception.
			self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 1},
	)
	def test_booking_overlap_when_bigger(self):
		dt_start2 = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_start1 = add_to_date(dt_start2, hours=1)  # 9:00
		dt_end1 = add_to_date(dt_start1, hours=1)  # 10:00
		dt_end2 = add_to_date(dt_end1, hours=1)  # 11:00
		booking_1 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start1, dt_end1)

		with self.assertRaises(frappe.ValidationError):
			# Here we try to book "around" the existing booking.
			booking_2 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start2, dt_end2)

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 1},
	)
	def test_booking_no_overlap_with_start_equals_end(self):
		dt_start1 = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_end1 = add_to_date(dt_start1, hours=1)  # 9:00
		dt_start2 = dt_end1  # 9:00
		dt_end2 = add_to_date(dt_start2, hours=1)  # 10:00
		booking_1 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start1, dt_end1)
		booking_2 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start2, dt_end2)

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 1},
	)
	def test_booking_no_overlap_with_start_equals_end_reversed(self):
		dt_start1 = add_to_date(getdate(), days=1, hours=8, minutes=0)  # 8:00
		dt_end1 = add_to_date(dt_start1, hours=1)  # 9:00
		dt_start2 = dt_end1  # 9:00
		dt_end2 = add_to_date(dt_start2, hours=1)  # 10:00
		booking_2 = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_start2, dt_end2
		)  # order is reversed
		booking_1 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start1, dt_end1)

	@change_settings(
		"Venue Settings",
		{"minute_uom": "Minute", "enable_simultaneous_booking": 0, "no_overlap_per_item": 0},
	)
	def test_all_kinds_of_bookings(self):
		from contextlib import contextmanager

		@contextmanager
		def make(dt_start, dt_end):
			booking = self.makeBooking(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)  # Create a booking
			yield booking
			booking.delete()

		"""
			6  7  8  9 10~16 17 18 19 20 Timeline
			......|______________|...... Overlap?
			...AAAAAAA..BBBB..CCCCCCC... yes
			......DDDD........EEEE...... yes
			......FFFFFFFFFFFFFFFF...... yes
			...GGGGGGGGGGGGGGGGGGGGGG... yes
			...XXXX..............WWWW... NO
			......U..............V...... NO
			ZZZZ....................YYYY NO
		"""

		cases = [
			# hour start, hour end, expected number of availabilities (10)
			(7, 9, "=", 9),
			(10, 16, "=", 4),
			(17, 19, "=", 9),
			(8, 9, "=", 9),
			(17, 18, "=", 9),
			(8, 18, "=", 0),
			(7, 19, "=", 0),
			(7, 8, "=", 10),
			(18, 19, "=", 10),
			(8, 8, "=", 10),
			(18, 18, "=", 10),
			(6, 7, "=", 10),
			(19, 20, "=", 10),
		]

		dt_now = add_to_date(getdate(), days=2, hours=0, minutes=0, seconds=0)

		def t(hour):
			return add_to_date(dt_now, hours=hour)

		for start, end, op, expected in cases:
			try:
				with make(t(start), t(end)) as b:
					availabilities = get_availabilities(
						self.ITEM_BOOKABLE_1.name, start=dt_now, end=add_to_date(dt_now, days=1), uom="Hour"
					)
					self.assertEqual(len(availabilities), expected)  # 10 hours between 8:00-18:00
					break
			except Exception:
				print("Case:", start, end, op, expected)
				raise

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_availability_on_customer_interface(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name,
			start=dt_start,  # NOTE: Only the date is taken into account
			end=dt_end,
			uom="Hour",
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00

		for i, a in enumerate(availabilities):
			start: str = a["start"]
			end: str = a["end"]
			taken: int = a["number"]
			avail: int = a["total_available"]
			self.assertTrue(start.endswith(str(i + 8).rjust(2, "0") + ":00:00"))
			self.assertTrue(end.endswith(str(i + 8 + 1).rjust(2, "0") + ":00:00"))
			self.assertEqual(taken, 0)
			self.assertEqual(avail, MAX_SIMULTANEOUS_BOOKINGS_1)

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_availability_in_the_future(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_availability_for_whole_slots(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=7)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_3.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 7)  # 7 slots in the week

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_availabilities_exceptional_closing_opening(self):
		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="2777-01-01", end="2777-01-02", uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00, normal, before

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="2900-01-01", end="2900-01-02", uom="Hour"
		)
		self.assertEqual(len(availabilities), 0)  # 0 hours because of exceptional closing

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="2901-01-01", end="2901-01-02", uom="Hour"
		)
		self.assertEqual(len(availabilities), 2)  # 2 hours between 10:00-12:00, exceptional opening

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="2901-01-02", end="2902-01-03", uom="Hour"
		)
		# TODO: exceptional opening should follow the normal calendar
		self.assertEqual(len(availabilities), 24)
		# self.assertEqual(len(availabilities), 10)  # 10 hours between 08:00-18:00

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="2903-01-01", end="2903-01-02", uom="Hour"
		)
		self.assertEqual(len(availabilities), 0)  # 0 hours because of exceptional closing

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_2.name, start="4000-01-01", end="4000-01-02", uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00, normal, after

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_booking_cancellation(self):
		from erpnext.venue.doctype.item_booking.item_booking import cancel_appointment, end_booking_now

		@contextmanager
		def rollbacker():
			frappe.db.savepoint("TestItemBooking_test_booking_cancellation")
			frappe.set_user("test@example.com")
			yield
			frappe.set_user("Administrator")
			frappe.db.rollback(save_point="TestItemBooking_test_booking_cancellation")

		past1 = add_to_date(getdate(), days=-4, hours=7)
		past2 = add_to_date(past1, hours=5)
		present1 = add_to_date(get_datetime(), hours=-3)
		present2 = add_to_date(get_datetime(), hours=3)
		future1 = add_to_date(getdate(), days=4, hours=7)
		future2 = add_to_date(future1, hours=5)

		frappe.set_user("Administrator")
		past = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, past1, past2, user="test@example.com"
		)
		present = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, present1, present2, user="test@example.com"
		)
		future = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, future1, future2, user="test@example.com"
		)

		with rollbacker():
			self.assertRaises(frappe.ValidationError, end_booking_now, past.name)
			end_booking_now(present.name)
			self.assertRaises(frappe.ValidationError, end_booking_now, future.name)

		with rollbacker():
			frappe.set_user("test2@example.com")
			self.assertRaises(frappe.PermissionError, end_booking_now, present.name)

		with change_settings("Venue Settings", {"allow_event_cancellation": 0, "cancellation_delay": 0}):
			with rollbacker():
				self.assertRaises(frappe.ValidationError, cancel_appointment, past.name)
				self.assertRaises(frappe.ValidationError, cancel_appointment, present.name)
				self.assertRaises(frappe.ValidationError, cancel_appointment, future.name)

		with change_settings("Venue Settings", {"allow_event_cancellation": 1, "cancellation_delay": 0}):
			with rollbacker():
				self.assertRaises(frappe.ValidationError, cancel_appointment, past.name)
				self.assertRaises(frappe.ValidationError, cancel_appointment, present.name)
				cancel_appointment(future.name)
			with rollbacker():
				frappe.set_user("test2@example.com")
				self.assertRaises(frappe.PermissionError, cancel_appointment, future.name)

		with change_settings(
			"Venue Settings", {"allow_event_cancellation": 1, "cancellation_delay": 9999999999999}
		):
			with rollbacker():
				self.assertRaises(frappe.ValidationError, cancel_appointment, past.name)
				self.assertRaises(frappe.ValidationError, cancel_appointment, present.name)
				self.assertRaises(frappe.ValidationError, cancel_appointment, future.name)

		frappe.set_user("Administrator")

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_booking_web_list(self):
		from erpnext.venue.doctype.item_booking.item_booking import (
			get_bookings_list,
			get_bookings_list_for_map,
		)

		def check_results(user: str, n1: int, n2: int):
			frappe.set_user(user)
			res1 = get_bookings_list(
				"Item Booking", "", {}, limit_start=0, limit_page_length=20, order_by=None
			)
			self.assertEqual(len(res1), n1)
			res2 = get_bookings_list_for_map(getdate(), add_to_date(getdate(), days=7))
			self.assertEqual(len(res2), n2)

		frappe.db.delete("Item Booking", {})  # Clear all bookings
		check_results("Administrator", 0, 0)
		check_results("test@example.com", 0, 0)

		frappe.set_user("Administrator")
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, hours=5)
		# Can be seen in the web list, only by test@example.com
		booking1 = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_start, dt_end, user="test@example.com"
		)
		# Cannot be seen in the web list
		booking2 = self.makeBookingWithAutocleanup(self.ITEM_BOOKABLE_1.name, dt_start, dt_end)

		check_results("Administrator", 0, 0)  # Even Administrator cannot see these results
		check_results("Guest", 0, 0)
		check_results("test@example.com", 1, 1)
		check_results("test2@example.com", 0, 0)

		frappe.set_user("Administrator")
		frappe.db.rollback()


class TestItemBookingWithSubscription(BaseTestWithSubscriptionForBookableItem):
	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_subscription_decreases_availability1(self):
		dt_sub_start = add_to_date(getdate(), days=2)
		subscription = self.makeSubscription(start_date=dt_sub_start, item=self.ITEM_SUB_1.name)
		self.addCleanup(subscription.delete)

		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_subscription_decreases_availability2(self):
		dt_sub_start = add_to_date(getdate(), days=2)
		subscription1 = self.makeSubscription(start_date=dt_sub_start, item=self.ITEM_SUB_1.name)
		subscription2 = self.makeSubscription(start_date=dt_sub_start, item=self.ITEM_SUB_1.name)
		self.addCleanup(subscription1.delete)
		self.addCleanup(subscription2.delete)

		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 0)

	@change_settings(
		"Venue Settings",
		{
			"minute_uom": "Minute",
			# NOTE: Disable simultaneous booking (so that only 1 subscription is needed to take all the booking slots)
			"enable_simultaneous_booking": 0,
		},
	)
	def test_successive_subscription_decreases_availability(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_mid = add_to_date(dt_start, days=1)
		dt_end = add_to_date(dt_start, days=2)

		subscription = self.makeSubscription(start_date=dt_mid, item=self.ITEM_SUB_1.name)
		self.addCleanup(subscription.delete)

		availabilities1 = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_mid, uom="Hour"
		)
		self.assertEqual(len(availabilities1), 10)  # 10 hours between 8:00-18:00

		availabilities2 = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_mid, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities2), 0)  # subscription started

		availabilities_full = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities_full), 10)  # 10 hours of the first period

	@change_settings(
		"Venue Settings",
		{
			"minute_uom": "Minute",
			"enable_simultaneous_booking": 0,
		},
	)
	def test_cancelled_subscription(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		subscription = self.makeSubscription(start_date=dt_start, item=self.ITEM_SUB_1.name)
		self.addCleanup(subscription.delete)

		availabilities1 = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities1), 0)  # The subscription is active

		subscription.cancel_subscription(cancellation_date=dt_start.date())

		availabilities2 = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities2), 10)  # The subscription was cancelled

	@change_settings(
		"Venue Settings",
		{
			"minute_uom": "Minute",
			"enable_simultaneous_booking": 0,
		},
	)
	def test_subscription_just_started_and_just_ended(self):
		dt_base = add_to_date(getdate(), days=4, hours=7)  # Reference
		dt_sub_start = add_to_date(dt_base, days=1)
		dt_sub_end = add_to_date(dt_base, days=2)
		dt_end = add_to_date(dt_base, days=3)

		def count_slots(start, end):
			return (end - start).days * 10  # 10 slots per day

		assert count_slots(dt_base, dt_end) == 30, "invariant failed: date difference is not correct"
		assert (
			count_slots(dt_base, dt_sub_start) == 10
		), "invariant failed: date difference is not correct"
		assert (
			count_slots(dt_sub_start, dt_sub_end) == 10
		), "invariant failed: date difference is not correct"
		assert count_slots(dt_sub_end, dt_end) == 10, "invariant failed: date difference is not correct"

		n_slots1 = count_slots(dt_base, dt_sub_start)
		availabilities1 = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_base, end=dt_sub_start, uom="Hour"
		)
		self.assertEqual(len(availabilities1), n_slots1)  # subscription does not even exist

		subscription = self.makeSubscription(start_date=dt_sub_start, item=self.ITEM_SUB_1.name)
		self.addCleanup(subscription.delete)
		subscription.cancel_subscription(cancellation_date=dt_sub_end.date())

		self.assertEqual(
			len(get_availabilities(self.ITEM_BOOKABLE_1.name, start=dt_base, end=dt_sub_start, uom="Hour")),
			count_slots(dt_base, dt_sub_start),
		)  # subscription not started yet

		self.assertEqual(
			len(
				get_availabilities(self.ITEM_BOOKABLE_1.name, start=dt_sub_start, end=dt_sub_end, uom="Hour")
			),
			0,
		)  # subscription just started and ended

		self.assertEqual(
			len(get_availabilities(self.ITEM_BOOKABLE_1.name, start=dt_sub_end, end=dt_end, uom="Hour")),
			count_slots(dt_sub_end, dt_end),
		)  # subscription already ended

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 0})
	def test_availability_with_non_booking_subscription(self):
		dt_sub_start = getdate()
		dt_start = add_to_date(dt_sub_start, days=4, hours=7)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00

		subscription = self.makeSubscription(
			start_date=dt_sub_start, item=self.ITEM_SUB_1.name, item_booking=False
		)
		self.addCleanup(subscription.delete)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour"
		)
		self.assertEqual(len(availabilities), 10)  # 10 hours between 8:00-18:00

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 0})
	def test_availability_in_cart(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_book_start = add_to_date(dt_start, hours=5)  # 7 + 5 = 12
		dt_book_end = add_to_date(dt_book_start, hours=1)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 10)

		booking = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_book_start, dt_book_end, TEST_USER_1, status="In cart"
		)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 9 + [1])

		for avail in availabilities:
			start = frappe.utils.get_datetime(avail["start"])
			end = frappe.utils.get_datetime(avail["end"])
			if start == dt_book_start and end == dt_book_end:
				self.assertEqual(avail["status"], "selected")
				self.assertEqual(avail["number"], 1)
			else:
				self.assertEqual(avail["status"], "available")
				self.assertEqual(avail["number"], 0)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_2
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 9)  # a spot is taken by TEST_USER_1

		for avail in availabilities:
			self.assertEqual(avail["status"], "available")
			self.assertEqual(avail["number"], 0)

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 0})
	def test_availability_for_specific_user(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_book_start = add_to_date(dt_start, hours=5)  # 7 + 5 = 12
		dt_book_end = add_to_date(dt_book_start, hours=1)
		dt_end = add_to_date(dt_start, days=1)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 10)

		booking = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_book_start, dt_book_end, TEST_USER_1
		)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 9 + [1])

		for avail in availabilities:
			start = frappe.utils.get_datetime(avail["start"])
			end = frappe.utils.get_datetime(avail["end"])
			if start == dt_book_start and end == dt_book_end:
				self.assertEqual(avail["status"], "confirmed")
				self.assertEqual(avail["number"], 1)
			else:
				self.assertEqual(avail["status"], "available")
				self.assertEqual(avail["number"], 0)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_2
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 9)  # a spot is taken by TEST_USER_1

		for avail in availabilities:
			self.assertEqual(avail["status"], "available")
			self.assertEqual(avail["number"], 0)

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 0})
	def test_availability_for_specific_user_multiple_hours(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_book_start = add_to_date(dt_start, hours=5)  # 7 + 5 = 12
		dt_book_end = add_to_date(dt_book_start, hours=3)
		dt_end = add_to_date(dt_start, days=1)

		n_hours_taken = int((dt_book_end - dt_book_start).total_seconds() / 3600)
		self.assertEqual(n_hours_taken, 3, "invariant failed: date difference is not correct")

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 10)

		booking = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_book_start, dt_book_end, TEST_USER_1
		)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(Counter(numbers), {0: 10 - n_hours_taken, 1: 1})

		for avail in availabilities:
			start = frappe.utils.get_datetime(avail["start"])
			end = frappe.utils.get_datetime(avail["end"])
			if start == dt_book_start and end == dt_book_end:
				self.assertEqual(avail["status"], "confirmed")
				self.assertEqual(avail["number"], 1)
			else:
				self.assertEqual(avail["status"], "available")
				self.assertEqual(avail["number"], 0)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_2
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * (10 - n_hours_taken))

		for avail in availabilities:
			self.assertEqual(avail["status"], "available")
			self.assertEqual(avail["number"], 0)

	@change_settings("Venue Settings", {"minute_uom": "Minute", "enable_simultaneous_booking": 1})
	def test_availability_for_specific_user_with_simultaneous(self):
		dt_start = add_to_date(getdate(), days=4, hours=7)
		dt_book_start = add_to_date(dt_start, hours=5)  # 7 + 5 = 12
		dt_book_end = add_to_date(dt_book_start, hours=1)
		dt_end = add_to_date(dt_start, days=1)

		n_hours_taken = int((dt_book_end - dt_book_start).total_seconds() / 3600)
		self.assertEqual(n_hours_taken, 1, "invariant failed: date difference is not correct")

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 10)

		booking1 = self.makeBookingWithAutocleanup(
			self.ITEM_BOOKABLE_1.name, dt_book_start, dt_book_end, TEST_USER_1
		)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 4 + [1] + [0] * 5)  # 5 free before, 1 taken, 4 free after

		for avail in availabilities:
			start = frappe.utils.get_datetime(avail["start"])
			end = frappe.utils.get_datetime(avail["end"])
			if start == dt_book_start and end == dt_book_end:
				self.assertEqual(avail["status"], "confirmed")
				self.assertEqual(avail["number"], 1)
			else:
				self.assertEqual(avail["status"], "available")
				self.assertEqual(avail["number"], 0)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_2
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 10)

		for avail in availabilities:
			self.assertEqual(avail["status"], "available")
			self.assertEqual(avail["number"], 0)

		for i in range(MAX_SIMULTANEOUS_BOOKINGS_1 - 1):
			bookingN = self.makeBookingWithAutocleanup(
				self.ITEM_BOOKABLE_1.name, dt_book_start, dt_book_end, TEST_USER_1
			)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_1
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(Counter(numbers), Counter({0: 9, 2: 1}))  # 9 free total, 1 taken twice

		for avail in availabilities:
			start = frappe.utils.get_datetime(avail["start"])
			end = frappe.utils.get_datetime(avail["end"])
			if start == dt_book_start and end == dt_book_end:
				self.assertEqual(avail["status"], "confirmed")
				self.assertEqual(avail["number"], 2)
			else:
				self.assertEqual(avail["status"], "available")
				self.assertEqual(avail["number"], 0)

		availabilities = get_availabilities(
			self.ITEM_BOOKABLE_1.name, start=dt_start, end=dt_end, uom="Hour", user=TEST_USER_2
		)
		numbers = list(map(itemgetter("number"), availabilities))
		self.assertEqual(numbers, [0] * 9)

		for avail in availabilities:
			self.assertEqual(avail["status"], "available")
			self.assertEqual(avail["number"], 0)
