frappe.listview_settings['Item Booking'] = {
	hide_name_column: true,
	get_indicator: function(doc) {
		if (doc.status == "Confirmed") {
			return [__("Confirmed"), "green", "status,=,Confirmed"];
		} else if (doc.status == "Cancelled") {
			return [__("Cancelled"), "red", "status,=,Cancelled"];
		} else if (doc.status == "In cart") {
			return [__("In cart"), "orange", "status,=,In cart"];
		} else if (doc.status == "Not confirmed") {
			return [__("Not confirmed"), "grey", "status,=,Not confirmed"];
		}
	},
	onload: function(list_view) {
		if (list_view.page.fields_dict.user) {
			list_view.page.fields_dict.user.get_query = function() {
				return {
					query: "frappe.core.doctype.user.user.user_query",
					filters: {ignore_user_type: 1}
				}
			};
		}

		const create_quotation = () => {
			const selected_docs = list_view.get_checked_items();
			const docnames = list_view.get_checked_items(true);

			if (selected_docs.length > 0) {
				for (let doc of selected_docs) {
					if (doc.docstatus == 2) {
						frappe.throw(__("Cannot create a quotation from Cancelled documents."));
					}
				}

			frappe.new_doc("Quotation")
				.then(() => {
					cur_frm.set_value("items", []);

					frappe.call({
						type: "POST",
						method: "frappe.model.mapper.map_docs",
						args: {
							"method": "erpnext.venue.doctype.item_booking.item_booking.make_quotation",
							"source_names": docnames,
							"target_doc": cur_frm.doc
						},
						callback: function (r) {
							if (!r.exc) {
								frappe.model.sync(r.message);
								cur_frm.dirty();
								cur_frm.refresh();
							}
						}
					});
				})
			}
		};

		list_view.page.add_action_item(__("Make a quotation"), create_quotation, false);
	}
};