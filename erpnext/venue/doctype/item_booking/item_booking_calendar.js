// Copyright (c) 2019, Dokos SAS and Contributors
// License: See license.txt

frappe.views.calendar["Item Booking"] = {
	field_map: {
		"start": "starts_on",
		"end": "ends_on",
		"id": "name",
		"title": "title",
		"allDay": "all_day",
		"rrule": "rrule",
		"color": "color",
		"status": "status",
		"doctype": "doctype"
	},
	status_color: {
		"In cart": "orange",
		"Not confirmed": "darkgrey",
		"Confirmed": "green",
		"Cancelled": "red"
	},
	filters: [["Item Booking", "status", "!=", "Cancelled"]],
	get_events_method: "erpnext.venue.doctype.item_booking.item_booking.get_events_for_calendar",
	calendar_print_config: {
		no_title: true,
		shown_filter: "item",
		Planning: {
			no_title: true,
			shown_filter: null,
		},
	},
	default_resource: "item",
	resourceView: {
		eventDidMount: (view, info) => {
			const doctype = info.event.extendedProps.doctype ?? "Item Booking";
			const docname = info.event.extendedProps.name;
			info.el.dataset.doctype = doctype;
			info.el.dataset.name = docname;
			return true;
		},
		resourceCellContent: (view, arg) => {
			if (view.resource !== "item") return;

			const title = document.createElement("a");
			const capa = document.createElement("div");
			const nextAvail = document.createElement("div");
			nextAvail.classList.add("text-muted", "small");

			title.textContent = arg.resource.title;
			title.href = frappe.utils.get_form_link("Item", arg.resource.id);
			nextAvail.innerHTML = "&nbsp;";
			capa.innerHTML = `<span class=text-muted>${__("Loading...")}</span>`;

			const clear = () => {
				capa.innerHTML = "";
				nextAvail.innerHTML = "";
			}

			const load = async () => {
				const item_code = arg.resource.id;
				if (item_code === "null") {
					return clear();
				}
				// const doc = await frappe.model.with_doc("Item", item_code);
				const now = frappe.datetime.now_date();
				const avails = await frappe.call("erpnext.venue.doctype.item_booking.item_booking.get_availabilities", {
					start: now,
					end: frappe.datetime.add_days(now, 7),
					item: item_code,
					uom: "",
					user: "*",
					limit: 24,
				});

				const current = await frappe.xcall("erpnext.venue.doctype.item_booking.item_booking.get_booking_count", { item: item_code });

				clear();

				avails?.message?.sort((a, b) => {
					return String(a.start || a.starts_on).localeCompare(b.start || b.starts_on);
				});
				const availability = avails?.message?.find(a => a.status === "available");
				let formatted = "";
				if (availability) {
					const start = frappe.datetime.convert_to_system_tz(availability.start || availability.starts_on);
					const formattedDate = frappe.format(start, { fieldtype: "Datetime" });
					formatted = formattedDate;
				} else {
					formatted = __("Unavailable");
				}
				nextAvail.textContent = formatted;

				if (current) {
					let color = "green";
					if (current.current >= current.capacity) {
						color = "red";
					} else if (current.current >= current.capacity * 0.8) {
						color = "yellow";
					}
					capa.innerHTML = `<div class="indicator-pill whitespace-nowrap small no-indicator-dot user-select-none ${color}">
						${frappe.utils.icon("uil uil-users-alt", "xs")}
						&nbsp;
						${current.current} / ${current.capacity}
					</div>`;
				}
			}

			load();
			return { domNodes: [title, capa, nextAvail] };
		}
	}
};

frappe.realtime.off("booking_overlap");
frappe.realtime.on("booking_overlap", () => {
	frappe.show_alert({
		message: __("This item booking is overlapping with another item booking for the same item"),
		indicator: "orange"
	})
});