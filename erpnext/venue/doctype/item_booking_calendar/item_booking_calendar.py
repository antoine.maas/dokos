# Copyright (c) 2019, Dokos SAS and contributors
# For license information, please see license.txt


import datetime
from collections.abc import Iterable
from functools import cached_property, partial
from typing import TYPE_CHECKING, Literal, NamedTuple

import frappe
from frappe.model.document import Document
from frappe.utils import get_datetime, get_time, getdate

if TYPE_CHECKING:
	from erpnext.venue.doctype.item_booking_calendar_exception.item_booking_calendar_exception import (
		ItemBookingCalendarException,
	)


class ItemBookingCalendar(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from erpnext.venue.doctype.item_booking_calendar_exception.item_booking_calendar_exception import (
			ItemBookingCalendarException,
		)
		from erpnext.venue.doctype.item_booking_calendars.item_booking_calendars import (
			ItemBookingCalendars,
		)

		booking_calendar: DF.Table[ItemBookingCalendars]
		calendar_title: DF.Data
		exceptions: DF.Table[ItemBookingCalendarException]
		item: DF.Link | None
		uom: DF.Link | None
	# end: auto-generated types

	def clear_cache(self):
		super().clear_cache()
		frappe.cache.hdel("item_booking_exceptions", self.name)


class ItemBookingExceptionEngine:
	class ParsedEx(NamedTuple):
		type: Literal["+", "-"]
		dt_start: datetime.datetime
		dt_end: datetime.datetime

	class ExDateTimeTuple(NamedTuple):
		start_date: datetime.date
		end_date: datetime.date
		start_time: datetime.time
		end_time: datetime.time

	def __init__(self, cal_name: str) -> None:
		self.cal_name = cal_name

	@cached_property
	def cal(self) -> "ItemBookingCalendar":
		return frappe.get_doc("Item Booking Calendar", self.cal_name)  # type: ignore

	def parse_to_time_tuple(self, row: "ItemBookingCalendarException"):
		if not row.start_date:
			return None

		start_date = getdate(row.start_date)
		end_date = getdate(row.end_date) if row.end_date else start_date
		assert start_date and end_date  # make the typechecker happy

		# First, we parse the start and end times (hh:mm:ss)
		start_time: datetime.time | None = None
		end_time: datetime.time | None = None

		if s := row.get("start_time"):
			start_time = get_time(str(s)) if s else None
		if s := row.get("end_time"):
			end_time = get_time(str(s)) if s else None

		start_time = start_time or datetime.time.min
		end_time = end_time or datetime.time.max

		return ItemBookingExceptionEngine.ExDateTimeTuple(start_date, end_date, start_time, end_time)

	def transform_inclusive_to_exclusive_end(self, tup: ExDateTimeTuple) -> ExDateTimeTuple:
		"""
		Transforms an exclusive tuple to an inclusive tuple when the end time is 23:59:59.
		Examples: (2021-01-01, 2021-01-01, 00:00:00, 23:59:59) -> (2021-01-01, 2021-01-02, 00:00:00, 00:00:00)
		"""
		if tup.end_time == datetime.time.max:
			return ItemBookingExceptionEngine.ExDateTimeTuple(
				tup.start_date, tup.end_date + datetime.timedelta(days=1), tup.start_time, datetime.time.min
			)
		return tup

	def convert_tup_to_parsed_ex(self, op: Literal["+", "-"], tup: ExDateTimeTuple) -> ParsedEx:
		return ItemBookingExceptionEngine.ParsedEx(
			op,
			datetime.datetime.combine(tup.start_date, tup.start_time),
			datetime.datetime.combine(tup.end_date, tup.end_time),
		)

	def grab_from_document(
		self, source_doctype: str | None, source_name: str | None, source_field: str | None
	):
		if not (source_doctype and source_name and source_field):
			return
		try:
			src_doc = frappe.get_doc(source_doctype, source_name)
		except frappe.DoesNotExistError:
			return
		source = src_doc.get(source_field)
		if not source or not isinstance(source, Iterable):
			return
		date_fields = source[0].meta.get("fields", {"fieldtype": "Date"})
		if not date_fields:
			return
		start_field: str = date_fields[0].fieldname  # type: ignore

		for item in source:
			start = item.get(start_field)
			if not start:
				continue
			tup = ItemBookingExceptionEngine.ExDateTimeTuple(
				getdate(start),  # type: ignore
				getdate(start),  # type: ignore
				datetime.time.min,
				datetime.time.max,
			)
			tup = self.transform_inclusive_to_exclusive_end(tup)
			yield tup

	def get_operations(self) -> list[ParsedEx]:
		"""Get the list of additions/exclusions for the full schedule."""
		if not self.cal_name:
			return []

		from_cache = frappe.cache.hget("item_booking_exceptions", self.cal_name)
		if from_cache:
			return from_cache

		if not self.cal.get("exceptions"):
			return []

		out: list[ItemBookingExceptionEngine.ParsedEx] = []
		for row in self.cal.exceptions:
			op = "+" if row.type.startswith("Add") else "-"

			if row.type in ("Add", "Remove"):
				# Add or remove a single day, or range of days
				# If end date is not set, it defaults to the same value as start date (i.e. a single day)
				tup = self.parse_to_time_tuple(row)
				if not tup:
					continue
				tup = self.transform_inclusive_to_exclusive_end(tup)
				out.append(self.convert_tup_to_parsed_ex(op, tup))
			elif row.type.endswith("(from a list)"):
				items = self.grab_from_document(row.source_doctype, row.source_name, row.source_field)
				mapper = partial(self.convert_tup_to_parsed_ex, op)
				out.extend(map(mapper, items))

		frappe.cache.hset("item_booking_exceptions", self.cal_name, out)
		return out

	def filter_from_op(self, op: ParsedEx, slots: list):
		"""Filter the given slots according to the given operation."""
		if op.type != "-":
			return slots
		return [
			slot for slot in slots if not self.check_overlap(op.dt_start, op.dt_end, slot.start, slot.end)
		]

	def filter_keep_in_range(
		self, range_start: datetime.datetime, range_end: datetime.datetime, slots: list
	):
		return [
			slot for slot in slots if self.check_contain(range_start, range_end, slot.start, slot.end)
		]

	def check_overlap(
		self,
		range_start: datetime.datetime,
		range_end: datetime.datetime,
		start: datetime.datetime | str | None,
		end: datetime.datetime | str | None,
	) -> bool:
		"""Check if a slot is overlapping a range."""
		if isinstance(start, str):
			start = get_datetime(start)
		if isinstance(end, str):
			end = get_datetime(end)
		if not (start and end):
			return False

		if end >= range_start and start <= range_end:
			return True
		return False

	def check_contain(
		self,
		range_start: datetime.datetime,
		range_end: datetime.datetime,
		start: datetime.datetime | str | None,
		end: datetime.datetime | str | None,
	) -> bool:
		"""Check if a slot is included in a range."""
		if isinstance(start, str):
			start = get_datetime(start)
		if isinstance(end, str):
			end = get_datetime(end)
		if not (start and end):
			return False

		if start >= range_start and end <= range_end:
			return True
		return False
