# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

from frappe.model.document import Document


class VenueUnitsofMeasure(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		duration: DF.Duration
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		unit_of_measure: DF.Link
	# end: auto-generated types

	pass
