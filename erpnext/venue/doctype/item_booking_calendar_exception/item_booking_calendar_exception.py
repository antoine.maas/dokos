# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class ItemBookingCalendarException(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		end_date: DF.Date | None
		end_time: DF.Time | None
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		source_doctype: DF.Link | None
		source_field: DF.Data | None
		source_name: DF.DynamicLink | None
		start_date: DF.Date | None
		start_time: DF.Time | None
		type: DF.Literal["Remove", "Add", "Remove (from a list)", "Add (from a list)"]
	# end: auto-generated types
	pass
