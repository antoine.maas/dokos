# Copyright (c) 2024, Dokos SAS and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document


class VenueUOMConversion(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		from_uom: DF.Link
		parent: DF.Data
		parentfield: DF.Data
		parenttype: DF.Data
		target_type: DF.Literal["Month", "Week", "Day"]
		to_uom: DF.Link | None
		value: DF.Int
	# end: auto-generated types

	pass
