frappe.ui.form.on("Customer", {
	setup(frm) {
		frm.set_query("company_search", function(doc) {
			return {
				query: "erpnext.regional.france.extensions.supplier.company_query"
			};
		});
	},

	company_search(frm) {
		if (frm.get_field("company_search")._data.length) {
			const selection = frm.get_field("company_search")._data.filter(f => f.value == frm.doc.company_search)

			if (selection.length){
				const selected_company = selection[0]
				frm.set_value("customer_name", selected_company.label)
				frm.set_value("siren_number", selected_company.value)
			}
		}
	}

})