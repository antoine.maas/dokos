import click
import frappe


def execute():
	if "webshop" in frappe.get_installed_apps():
		return

	if not frappe.db.table_exists("Website Item"):
		return

	doctypes = [
		"E Commerce Settings",
		"Website Item",
		"Recommended Items",
		"Item Review",
		"Wishlist Item",
		"Wishlist",
		"Website Offer",
		"Website Item Tabbed Section",
	]

	for doctype in doctypes:
		frappe.delete_doc("DocType", doctype, ignore_missing=True)

	frappe.delete_doc("Workspace", "E Commerce", ignore_missing=True, force=True)

	click.secho(
		"E-commerce features were moved to a separate app.\n"
		"Please install the Webshop app to continue using e-commerce features: https://gitlab.com/dokos/webshop",
		fg="yellow",
	)
