import frappe


def execute():
	mop_map = {
		x.payment_gateway: x.name
		for x in frappe.get_all("Mode of Payment", fields=["payment_gateway", "name"])
	}

	for payment_request in frappe.get_all(
		"Payment Request", fields=["name", "mode_of_payment", "payment_gateway"]
	):
		if not payment_request.mode_of_payment and payment_request.payment_gateway:
			frappe.db.set_value(
				"Payment Request",
				payment_request.name,
				"mode_of_payment",
				mop_map.get(payment_request.payment_gateway),
			)
