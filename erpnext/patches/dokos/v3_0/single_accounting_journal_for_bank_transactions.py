from itertools import groupby

import frappe
from frappe.utils import nowdate

from erpnext.accounts.general_ledger import make_entry, process_gl_map
from erpnext.accounts.utils import get_fiscal_year


def execute():
	payment_entries = frappe.get_all(
		"GL Entry",
		filters={"voucher_type": "Payment Entry", "fiscal_year": get_fiscal_year(nowdate())[0]},
		fields=[
			"accounting_entry_number",
			"name",
			"account",
			"accounting_journal",
			"credit",
			"debit",
			"voucher_no",
		],
	)
	party_accounts = frappe.get_all(
		"Account", filters={"account_type": ("in", ("Payable", "Receivable"))}, pluck="name"
	)
	for _, group in groupby(payment_entries, lambda x: x["accounting_entry_number"]):
		entries = list(group)
		if len(set([g.accounting_journal for g in entries])) == 1:
			continue

		if party_entries := [g.accounting_journal for g in entries if g.account in party_accounts]:
			party_journal = party_entries[0]
			for g in entries:
				if g.accounting_journal != party_journal:
					frappe.db.set_value("GL Entry", g.name, "accounting_journal", party_journal)

	for cancelled_state in [0, 1]:
		payment_entries = frappe.get_all(
			"GL Entry",
			filters={
				"voucher_type": "Payment Entry",
				"fiscal_year": get_fiscal_year(nowdate())[0],
				"is_cancelled": cancelled_state,
			},
			fields=[
				"accounting_entry_number",
				"name",
				"account",
				"accounting_journal",
				"credit",
				"debit",
				"voucher_no",
			],
		)
		for _, group in groupby(
			sorted(payment_entries, key=lambda x: x["accounting_entry_number"]),
			lambda x: x["accounting_entry_number"],
		):
			entries = list(group)
			debit = sum(x.debit for x in entries)
			credit = sum(x.credit for x in entries)
			if frappe.utils.flt(debit, 2) != frappe.utils.flt(credit, 2):
				doc = frappe.get_doc("Payment Entry", entries[0].voucher_no)
				gl_entries = []
				doc.add_deductions_gl_entries(gl_entries)
				gl_entries = process_gl_map(gl_entries)
				for gl_entry in gl_entries:
					gl_entry["accounting_entry_number"] = entries[0].accounting_entry_number
					gl_entry["accounting_journal"] = entries[0].accounting_journal
					make_entry(gl_entry, False, "False")
