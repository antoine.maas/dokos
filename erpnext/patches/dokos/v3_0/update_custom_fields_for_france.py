import frappe

from erpnext.regional.france.setup import setup_company_independent_fixtures


def execute():
	company = frappe.get_all("Company", filters={"country": "France"})
	if not company:
		return

	setup_company_independent_fixtures()
